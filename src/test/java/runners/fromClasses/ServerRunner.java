//package runners;
//
//import core.base.manegers.DriverManager;
//import core.listeners.errorCollector.ErrorCollector;
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//import org.testng.junit.JUnit4TestRunner;
//import org.testng.junit.JUnitTestRunner;
//import pagetests.FirstAnimationTest;
//import pagetests.LoginFromRightPage;
//import pagetests.LoginTest;
//import utilities.customlogger.Log;
//import utilities.customlogger.LogLevel;
//
//
//public class ServerRunner {
//
//    private static Log logger = new Log ();
//
//    public static JUnit4TestRunner runClass(){
//        JUnit4TestRunner classRunner = runClass();
//        return classRunner;
//    }
//
//    public static void main(String[]args){
//        try {
//            logger.writeToLogAndConsole(LogLevel.INFO,"About To Start Classes Test" + runClass().getTestMethods());
//            DriverManager.createDriver();
//            runClass().run(FirstAnimationTest.class);
//            runClass().run(LoginFromRightPage.class);
//            runClass().run(LoginTest.class);
//            ErrorCollector.getVerificationFailures();
//        }catch (NoClassDefFoundError ee){
//            logger.writeToLogAndConsole(LogLevel.INFO,"See Exception : " + ee.getMessage());
//        }
//    }
//}
