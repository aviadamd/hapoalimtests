//package fromClasses;
//
//import core.base.Base;
//import helper.actions.Commands;
//import helper.wait.WaitHelper;
//import org.openqa.selenium.NoSuchElementException;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//import pages.FirstAnimationsPage;
//import pagetests.FirstAnimationTest;
//import pagetests.LoginFromRightPage;
//import pagetests.LoginTest;
//import utilities.customlogger.Log;
//import utilities.customlogger.LogLevel;
//
//import java.io.IOException;
//
//public class TestSuite extends Base {
//
//    private FirstAnimationTest firstAnimationTest = new FirstAnimationTest ();
//    private LoginTest loginTest = new LoginTest ();
//    private LoginFromRightPage loginFromRightPage = new LoginFromRightPage ();
//    private final Commands commands = new Commands (Base.driver);
//    private final WaitHelper waitHelper = new WaitHelper (Base.driver);
//    private final Log logger = new Log ();
//
//
//    @BeforeTest
//    public void setUp() throws IOException {
//        initAppiumSession();
//    }
//
//    @Test
//    public void setFirstAnimationTest(){
//        try {
//            FirstAnimationsPage firstAnimationsPage = new FirstAnimationsPage (Base.driver);
//            if (waitHelper.waitForClickabilityOf (firstAnimationsPage.headerFirstHeadText, 30))
//                firstAnimationTest.animationFlow ();
//            else {
//                logger.writeToLogAndConsole (LogLevel.DEBUG, "User Has Already Pass First Animation Test Continue To LogIn");
//            }
//        }catch (NoSuchElementException no){
//            no.getMessage();
//        }
//    }
//
//    @Test
//    public void setLoginTest()throws IOException{
//        try {
//            loginTest.validLogin ();
//        }catch (NoSuchElementException no){
//            no.getMessage();
//        }
//    }
//
//    @Test
//    public void setLoginFromRightPage()throws IOException{
//        loginFromRightPage.testCheckDeposit();
//        loginFromRightPage.orderMatachTerminal();
//        loginFromRightPage.testCheckDeposit();
//        loginFromRightPage.testDrawFromAtm();
//        loginFromRightPage.testPari();
//        loginFromRightPage.testTransferMoney();
//    }
//
//}
