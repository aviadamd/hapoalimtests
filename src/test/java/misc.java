import com.google.common.collect.ImmutableList;
import helper.actions.Commands;
import helper.actionshelper.ActionsHelper;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.record.formula.functions.T;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static configs.GetDataFromJson.Url;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.testng.Assert.*;
import static pagetests.helper.AccountManagementUtilities.accountManagementUI;

public class misc {




    /**
     * Interface creates annotation MobileTest and specified parameters<br>
     * Annotation @MobileTest will let Grid know to open a session on a mobile device/simulator. This can be used both in
     * the Test method and the Test Class. When used in the class, then all the tests within the class share the same
     * session (Session Sharing)
     */
    @Retention(RUNTIME)
    @Target({ CONSTRUCTOR, METHOD, TYPE })
    public @interface MobileTest {
        /**
         * Establish application name to use. This is mandatory for all defaults apps. <b>Example</b>
         *
         * <pre>
         * &#064;Test()
         * &#064;MobileTest(appName = &quot;Safari&quot;)
         * public void webtest2() {
         *     Grid.open(&quot;http://paypal.com&quot;);
         * }
         * </pre>
         *
         * App version can be specified as part of the appName as:
         *
         * <pre>
         * appName = &quot;Safari:7.0&quot;
         * </pre>
         */
        String appName() default "";

        /**
         * Establish the type of device to be used (android, iphone, etc). No default. Platform version can be specified as
         * part of the device as:
         *
         * <pre>
         * device = &quot;iphone:7.1&quot;
         * </pre>
         */
        String device() default "";

        /**
         * Establish the application's language that is to be used. For example; <code>en</code> for English. Defaults to
         * the language chosen by Selendroid / ios-driver / Appium
         */
        String language() default "";

        /**
         * Establish the application's locale that is to be used. For example; <code>en_US</code> for US English. Defaults
         * to the locale chosen by Selendroid / ios-driver / Appium
         */
        String locale() default "";

        /**
         * Device Serial to be used. Defaults to the device/emulator chosen by Selendroid / ios-driver / Appium
         */
        String deviceSerial() default "";

        /**
         * Establish the type of device to be used (Nexus5, Iphone5s, etc). Defaults to the device/emulator chosen by
         * Selendroid / ios-driver / Appium
         */
        String deviceType() default "";

        /**
         * Provide additional capabilities that you may wish to add as a name value pair. Values of true or false will be
         * treated as Boolean capabilities unless you surround the value with {@code '}
         *
         * <pre>
         * {@literal @}Test
         * {@literal @}MobileTest(additionalCapabilities={"key1:value1","key2:value2"})
         * public void testMethod(){
         *     // flow
         * }
         * </pre>
         */
        String[] additionalCapabilities() default {};

        /**
         * This parameter represents the fully qualified path of the app that is to be spawned. For app exist in the local
         * disk this should be an absolute path, for app exist in the remote location it should be http URL and for app
         * exist in sauce cloud it can be sauce storage "sauce-storage:testApp.apk". This is mandatory for installable apps
         * running on Appium. <code>appPath</code> cannot be used along with the <code>appName</code>.
         *
         * <pre>
         *  for app in local disk it can be like appPath = C:\\test\\testApp.apk;
         * or for app in http location it can be like appPath = http://server/downloads/testApp.apk
         * or for app in sauce cloud it can be like appPath = sauce-storage:testApp.zip
         * </pre>
         */
        String appPath() default "";

        /**
         * This parameter specifies to execute mobile test cases using respective mobile driver.
         */
        String mobileNodeType() default "";

        /**
         * This parameter specifies the name of the browser
         *
         */

        String browserName() default "";
    }




    /**
     * annotation used to specify that the test method need to be ignored during reporting in RuntimeReporter.
     */
    @Retention(RUNTIME)
    @Target({METHOD})
    public @interface DoNotReport{
        String value() default "";
    }



    @Test
    public void outerTesst(String[] args) {
        // label for outer loop
        outer:
        for(int i = 0; i == 4; i++) {
            String y = "Hello";
            switch(y) {
                case "hhh":
                    break;
                case "Hello":
                    for(int a = 0; a == 4; a++) {
                        System.out.println("good");
                        break outer;
                    }
                case "shit":
                    break;
            }

        } // end of outer loop
    } // end of main()


    private WebDriver driver;


    public misc(WebDriver driver) {
        this.driver = driver;
    }

    private misc(){

    }

    public T[] arrayOf(T a,T b,T c) {
        T[] i = {a,b,c};
        return i;
    }

    private static <T> T[] getArr(T[] objects) {
        WebElement webElement = null;
        for(T newObjects : objects) {
            webElement.isDisplayed();
            int length = objects.length;
        }
        return objects;
    }


    private static <T extends Comparable<T>> T getMax(T[] objects,Boolean ifTrueGetMaxFalseIfMin) {
        T max = objects[0];
        if(ifTrueGetMaxFalseIfMin) {
            for(T object : objects) {
                if(object.compareTo(max) > 0)
                    max = object;
                return max;
            }
        } else {
            for(T object : objects) {
                if(object.compareTo(max) < 0)
                    max = object;
                   return max;
            }
        }
        return max;
    }



    @Test
    public void testPrintArr() {
        Double[] doubleArr = {1.4,7.2,4.1,3.7,1.1};
        String[] stringArr = {"hello","i","love","you"};
        Integer[] integersArr = {1,7,4,3,1};
        System.out.println(getMax(doubleArr,true));
        System.out.println(getMax(stringArr,true));
        System.out.println(getMax(integersArr,false));
    }


    public void verifyAfterDeleteCookieIfAlertIsUp(Cookie cookieName,List<WebElement> element) {
        Set<Cookie> allCookies = driver.manage().getCookies();

        if(allCookies.contains(cookieName)) {
            String cookiesNamesBeforeTest = cookieName.getPath() + cookieName.getDomain() + cookieName.getValue() + cookieName.getName();
            System.out.println("Cookies Information Before Testing - > " + cookiesNamesBeforeTest);
            driver.manage().deleteCookie(cookieName);

            if(!allCookies.isEmpty()) {
                System.out.println("Test Is Set For Testing After The Cookies Are Deleted");
                if(element.get(0).isDisplayed() || element.get(1).isDisplayed()) {
                    System.out.println("after delete cookies the alert is presented...   (: ");
                } else {
                    if(element.get(0) == null || element.get(1) == null) {
                        System.out.println("Very Bad After Delete The Cookies The Alerts Are Not Presented");
                    }
                }
            } else {
                String cookiesNamesAfterTest = cookieName.getPath() + cookieName.getDomain() + cookieName.getValue() + cookieName.getName();
                if(!cookiesNamesBeforeTest.equals(cookiesNamesAfterTest)) {
                    System.out.println("Cookies Information Cannot Be Deleted See Names - > " + cookieName.getPath() + cookieName.getDomain() + cookieName.getValue() + cookieName.getName());
                }
            }
        }
    }





    private void clickSevrelTimesWhileElementHeIsPresented(int timeToIrritate,WebElement whileHeIsPresented) {
        for(int r = 0; r <= timeToIrritate; r++) {
            try {
                while(whileHeIsPresented.isDisplayed()) {
                    whileHeIsPresented.toString().replace("1",String.valueOf(r));
                    System.out.println(whileHeIsPresented);
                }
            } catch(NullPointerException n) {
                n.getMessage();
                throw new NoSuchElementException("Sorry I Cant Find Your Element");
            }
        }
    }


    public void switchToActivity(Activity activity) {
        //switch to required activity
        try {
            ((AndroidDriver<WebElement>) driver).startActivity(activity);
        } catch(Exception e) {
            throw new NoSuchElementException("TODO...");
        }

    }

    private int size;
    public boolean isEmpty() {
        return size == 0;
    }

    private transient Object[] elementData;

    private int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++)
                if (elementData[i]==null)
                    return i;
                System.out.println("Working Fine");
        } else {
            for (int i = 0; i < size; i++)
                if (o.equals(elementData[i]))
                    return i;
        }
        return -1;
    }




    /**
     * Checks if property exists in the configuration
     *
     * @param// propertyName
     *            String Property Name
     * @return <b>true</b> or <b>false</b>
     */
//    public static boolean checkPropertyExists(String propertyName) {
//        checkArgument(propertyName != null, "Property name cannot be null");
//        return getConfig().containsKey(propertyName); //TODO
//    }



    private void verifyElementNotExists(List<String> element) {
        if(indexOf(element) == 0||isEmpty()) {
            Assert.assertEquals(element.isEmpty(),isEmpty());
            System.out.println("Work");
        }else fail("Try Again");
    }


    @Test
    public void testIndexOf(){
        List<String> list = new ArrayList<>();
        list.add("aaa");
        verifyElementNotExists(list);
    }


    public void verifyElementNotExists(By locator) {
        assertEquals(driver.findElements(locator).size(),0);
    }


    private void clickServelTimes(WebElement whileHeIsPresented,int timeToVerify,WebElement element) {

        boolean flag1 = whileHeIsPresented.isDisplayed();
        boolean flag = whileHeIsPresented.isEnabled();

        for(int i = 0; i == timeToVerify; i++) {
            if(flag1 || flag) {
                element.click();
            } else {
                return;
            }
        }

    }


    private List<WebElement> timesToClickOnElement(WebElement element,By by,WebElement webElement,int timesToClick) {
        boolean flag;
        List<WebElement> byIndex;
        byIndex = new ArrayList<>(element.findElements(By.id(by.toString())));

        if(byIndex.isEmpty()) {
            return null;
        } else {
            try {
                int i;
                for(i = 0; i <= timesToClick; i++) {
                    flag = webElement.isDisplayed();
                    if(flag) {
                        byIndex.get(timesToClick).click();
                    } else
                        throw new NullPointerException("TODO");
                }
            } catch(ArrayIndexOutOfBoundsException a) {
                a.getCause();
            }
        }
        return byIndex;
    }


    public static Object blaBla(Object aObject) {
        if(aObject instanceof String) {
            //TODO
        }
        if(null != aObject && aObject instanceof String) {
            //the long way
        }
        return aObject;
    }

    private static boolean isElementPresented(WebElement element) {
        return element.isDisplayed();
    }


    @Test
    public void withReferenceAndLambda() {
        List<WebElement> list = new ArrayList<>();
        list.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);
        list.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);
        list.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);

        //for each
        for(WebElement element : list) {
            isElementPresented(element);
        }

        //regular for
        for(int i = 0; i <= list.size(); i++) {
            isElementPresented(list.get(i));
        }

        //with while
        Iterator<WebElement> iterator = list.iterator();
        while(iterator.hasNext()) {
            isElementPresented(iterator.next());
        }

        list.forEach(misc::isElementPresented);//method reference
        list.forEach(item -> System.out.println(item)); //lamda
        list.forEach(System.out::println); //method reference
    }


    private Calendar verifyWithCalender() {
        Calendar calendar = GregorianCalendar.getInstance();
        return calendar;
    }

    @Test
    public void mvfvm() {
        for(int i = 0; i <= 5; i++) {
            System.out.println(getRandomNumber());
            //webElement.sendKeys(getRandomNumber());
        }
    }


    public static void clickRandomOnWebElements(WebElement w,WebElement s) {

        //one option
        WebElement[] webElements1 = {w,s};
        List<WebElement> listings = Arrays.asList(webElements1);
        Random r = new Random();
        int randomValue = r.nextInt(listings.size());
        listings.get(randomValue).click(); //Clicking on the random item in the list.

        //another option
        Random random = new Random();
        ImmutableList<WebElement> webElements = ImmutableList.<WebElement>builder().add(w,s).add(s).build();
        ImmutableList<WebElement> webElementList = ImmutableList.of(w,s); //with immutableList
        try {
            for(int i = 0; i <= webElements.size(); i++) {
                Commands.click(webElementList.get(random.nextInt(i))); //verify in other way
                Commands.click(webElementList.get(random.nextInt(2))); //verify in one way
                Commands.click(webElements.get(random.nextInt(i)));
            }
        } catch(Exception e) {
            throw new ArrayIndexOutOfBoundsException(e.getMessage());
        }


        //more why
        Iterator<WebElement> itr = webElementList.iterator();
        while(itr.hasNext()) {
            try {
                Commands.click(itr.next());
            } catch(Exception e) {
                e.printStackTrace();
            }

            WebElement element = null;
            int numOfCells = Integer.parseInt(element.getText());
            StringBuilder str2 = new StringBuilder(numOfCells);
            if(str2.capacity() == 10) {
                System.out.println(str2.capacity());
            }
        }
    }


    @Test
    public void tttt() {
        char s = 'a';
        StringBuilder str2 = new StringBuilder(s);
        System.out.println(str2.capacity());
    }


    private void getClickOnRandomElement(WebElement e,WebElement q,int numberOfRandomClicks) {

        int i;
        List<WebElement> list = new ArrayList<>();
        list.add(e);
        list.add(q);

        Random random = new Random();
        for(i = 0; i > numberOfRandomClicks; i++) {
            if(list.isEmpty())
                return;
            else
                list.get(random.nextInt(list.size())).click();
        }

    }

    private void vsvd(WebElement w,WebElement a) {
        WebElement[] elements = {w,a};
        //click on all elements
        List<WebElement> drop = Arrays.asList(elements);
        Iterator<WebElement> iterator = drop.iterator();
        while(iterator.hasNext()) {
            WebElement row = iterator.next();
            row.click();
        }

        //print all elements
        Iterator<WebElement> itr = drop.iterator();
        while(itr.hasNext()) {
            System.out.println(itr.next().getText());
        }


        String values = itr.next().getText();
        if(!values.equals("null")) {
            //todo
        } else {
            //todo
        }
    }


    public static int getRandomNumbers(int bonds) {
        Random rand = new Random();
        Integer next = rand.nextInt(bonds);
        return next;
    }


    @Test
    public void testRandomNumber() {

        int stop = 10;
        Random random = new Random();
        for(int i = 0; stop >= i; i++) {
            String randomNum = String.valueOf(random.nextInt(Integer.max(100,1000)));
            System.out.println(randomNum);
            System.out.println(randomNum);
        }
    }


    public static String getTimeStamp() {
        return new SimpleDateFormat("HH-mm").format(Calendar.getInstance().getTime());
    }


    public static String getRandomNumber() {
        Random rand = new Random();
        String next = String.valueOf(rand.nextInt(10));
        return next;
    }



    public boolean verifyTheNumberOfCellsWithInAWebElement(WebElement webElement,int numOfCells) {
        StringJoiner joinNames = new StringJoiner(webElement.getText());
        return joinNames.length() == numOfCells;
    }


    Random random = new Random();
    List<Integer> list = new ArrayList<>();

    public void setTheList() {
        list.add(311);
        list.add(33);
        list.add(23);
        list.add(10);
    }


    private Integer getRandomElement(List<Integer> list) {
        return list.get(random.nextInt(list.size()));
    }


    @Test
    public void testThisList() {
        System.out.println(getRandomElement(list));
    }

    @Test
    public void testHashMapOfWebElements() throws Exception{

        List<WebElement> valuesA = new ArrayList<>();
        valuesA.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);
        valuesA.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);
        valuesA.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);
        List<WebElement> valuesB = new ArrayList<>();
        valuesB.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);
        valuesB.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);
        valuesB.add(accountManagementUI.firstAnimationsPage.fourAnimationButton);

        WebElement mainView1 = accountManagementUI.firstAnimationsPage.fourAnimationButton;
        WebElement mainView2 = accountManagementUI.firstAnimationsPage.fourAnimationButton;

        HashMap<WebElement,List<WebElement>> hashMap = new HashMap<>();
        hashMap.put(mainView1,valuesA);
        hashMap.put(mainView2,valuesB);

        Set<WebElement> keys = hashMap.keySet();

        HashMap<Boolean,List<WebElement>> newHash = new HashMap<>();
        newHash.put(accountManagementUI.firstAnimationsPage.fourAnimationButton.isDisplayed(),valuesA);
        newHash.put(accountManagementUI.firstAnimationsPage.fourAnimationButton.isDisplayed(),valuesB);
        if(newHash.containsKey(true)) {
            for(int i =0; i <= newHash.size(); i++){
                newHash.get(i);
            }
        } else {
            throw new NullPointerException("TODO");
        }

        boolean key1 = accountManagementUI.loginPage.enterUserInput.isDisplayed();
        boolean key2 = accountManagementUI.loginPage.enterUserInput.isDisplayed();

        TreeMap<Boolean,List<WebElement>>treeMap = new TreeMap<>();
        treeMap.put(key1,valuesA);
        treeMap.put(key2,valuesB);

        for(Map.Entry<Boolean,List<WebElement>> entry : treeMap.entrySet()){
            if(entry.getKey().equals(key1)){
                Commands.click(entry.getValue().get(0));
            }
        }


        /**
         * try this
         */
        for(Map.Entry<WebElement,List<WebElement>> entry : hashMap.entrySet()) {
            if(Map.Entry.comparingByKey().equals(mainView1)) {
                if(valuesA.isEmpty()){
                    return;
                }else {
                    for(int i = 0; i <= valuesA.size(); i++) {
                        isElementPresented(entry.getValue().get(i));
                        isElementPresented(valuesA.get(i));
                    }
                }
            } else if(Map.Entry.comparingByKey().equals(mainView2)) {
                if(valuesB.isEmpty()){
                    return;
                }else {
                    for(int i = 0; i <= valuesA.size(); i++) {
                        isElementPresented(entry.getValue().get(i));
                        isElementPresented(valuesB.get(i));
                    }
                }
            } else {
                System.out.println("shit");
            }
        }
    }

    /**
     * work with tree map
     * @param arr
     */
    static void printFreq(int arr[]){

        // Creates an empty TreeMap
        TreeMap<Integer, Integer> tmap = new TreeMap<Integer, Integer>();

        // Traverse through the given array
        for (int i = 0; i < arr.length; i++) {
            Integer c = tmap.get(arr[i]);

            // If this is first occurrence of element
            if (tmap.get(arr[i]) == null)
                tmap.put(arr[i], 1);

                // If elements already exists in hash map
            else
                tmap.put(arr[i], ++c);
        }

        // Print result
        for (Map.Entry m:tmap.entrySet()){
            System.out.println("Frequency of " + m.getKey() +
                    " is " + m.getValue());
        }
    }

    public static void click(WebElement webElement){
         webElement.click();
    }

    public static String toLowerUnderscore(String upperCamel) {
        return Stream
                .of(upperCamel.split("(?=[A-Z])"))
                .map(s -> s.toLowerCase()).collect(Collectors.joining("_"));
    }

    @Test
    void fff(){
        final String s = toLowerUnderscore("fk.erjnfejkrf");

    }
    
    
    public static void main(String [] args) throws Exception{
        
       // toLowerUnderscore("kjdfvkjdnfv");
        
//        List<Boolean> names = Arrays.asList(
//                accountManagementUI.firstAnimationsPage.firstAnimationButton.isDisplayed(),
//                accountManagementUI.firstAnimationsPage.headerFirstHeadText.isDisplayed(),
//                accountManagementUI.loginPage.approveAccsessToMobileLocationAlert.isDisplayed()
//        );
//        List<Boolean> result = names.stream().filter(s->s.equals(true)).collect(Collectors.toList());
//        click((WebElement) result);
//
//
//
//        URL codefx;
//        codefx = new URL("https://www.bankhapoalim.co.il/wps/portal/PoalimPrivate/products?WCM_GLOBAL_CONTEXT=Poalim%20-%20Content/" +
//                "            poalimsite/sitearea_maintabs/sitearea_motsarimvesirutim/sitearea_accountmngmnt/niolheshboncha&proceed=1");
//        URLConnection connection;
//        connection = codefx.openConnection();
//        try(BufferedReader reader = new BufferedReader(
//                new InputStreamReader(connection.getInputStream()))){
//                    reader.lines().limit(5).
//                    forEach(System.out::println);
//        }
        //        // Creating KeyValue from List
//        List<String> list = new ArrayList<String>();
//        list.add("GeeksforGeeks");
//        list.add("A computer portal");

//        //array deque
//        Deque<String> deque = new ArrayDeque<>();
//        deque.add("hi");
//        deque.add("this");
//        deque.add("is");
//        deque.add("my");
//        deque.add("first");
//
//        for(String element : deque) {
//            System.out.println("Great I Can Use It In My Automation" + element);
//        }

//        if(deque.contains("is")) {
//            deque.add("hi im new element");
//            System.out.println(deque);
//        }
        //        EnumMap<GFG,String> gfgMap = new EnumMap<GFG,String>(GFG.class);
        //        gfgMap.put(GFG.CODE,"Start Coding with gfg");
        //        gfgMap.put(GFG.CONTRIBUTE,"Contribute for others");
        //        gfgMap.put(GFG.QUIZ,"Practice Quizes");
        //        gfgMap.put(GFG.MCQ,"Test Speed with Mcqs");
        //
        //        // Printing Java EnumMap
        //        // Print EnumMap in natural order
        //        // of enum keys (order on which they are declared)
        //        System.out.println("EnumMap: " + gfgMap);
        //
        //        // Retrieving value from EnumMap in java
        //        System.out.println("Key : " + GFG.CODE +" Value: " + gfgMap.get(GFG.CODE));
        //
        //
        //
        //        /* array lists of array list*/
        //        int NumberOfArrayCapacity = 3;
        //        ArrayList<ArrayList<String>> fatherList = new ArrayList<>(NumberOfArrayCapacity);
        //
        //        ArrayList<String> arrayList1 = new ArrayList<>();
        //        arrayList1.add("w");
        //        arrayList1.add("f");
        //        arrayList1.add("i");
        //        ArrayList<String> arrayList2 = new ArrayList<>();
        //        arrayList2.add("a");
        //        arrayList2.add("w");
        //        arrayList2.add("q");
        //
        //        fatherList.add(arrayList1);
        //        fatherList.add(arrayList2);
        //
        //        boolean verifyElement = fatherList.contains(arrayList1.get(2));
        //        boolean verifyElement1 = fatherList.contains(3);
        //
        //         //initWithForLoop get the father list size and enter it to another for
        //        // loop that take the i size
        //        for(int i =0; i < fatherList.size(); i++){
        //            for(int j = 0; j<fatherList.get(i).size(); j++){
        //                System.out.println(fatherList.get(i).get(j));
        //                if(verifyElement){
        //                    if(verifyElement1){
        //                        System.out.println("great");
        //                    }
        //                }
        //            }
        //        }
        //    }


    }
    /**
     * Create A Enum That Will Provide Key For EnumMap
     */
        public enum GFG {
        CODE, CONTRIBUTE, QUIZ, MCQ;
    }
}
