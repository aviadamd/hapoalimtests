package pagetests.helper;

public class Credentials{

    public static String manipulate(boolean ifTrueUpperCaseIfFalseLowerCase,String data){
        if (ifTrueUpperCaseIfFalseLowerCase){
            data.toUpperCase();
        }else {
            data.toLowerCase ();
        }
        return data;
    }


    public final static String MONEY = "100";
    public static String message0;
    public final static String message1 = "קוד משתמש";
    public final static String message2 = "סיסמה";
    public final static String message3 = "כניסה";
    public final static String message4 = "שחכתי סיסמה";
    public final static String firstHeader = "שמחים שהצטרפת לאפליקציית ניהול חשבון!";
    public final static String secondHeader = "להתעדכן בכלרגע ובכל מקום";
    public final static String secondBody = "כל מה שחשוב לך במצב החשבון : תנועות העוש כקטיסי האשראי פיקדונות מטח ועוד";
    public final static String thirdHeader  ="להעביר.להפקיד.להזמין";
    public final static String thirdBody = "מגוון פעולות רחב : העברת כספים משיכת מזומן מזומן מבנקט ללא כרטיס הפקדת שיק בצילום בקשת אשראי ברגע";
    public final static String fourHeader = "לבחור מה שמתאים לך";
    public final static String fourBody = "ניתן להיכנס לחשבונך איך שנוח לך : בטביעת אצבע בזיהוי קולי עם סיסמה";
    public final static String loginPage = "בוקר טוב כניסה לחשבונך";
}
