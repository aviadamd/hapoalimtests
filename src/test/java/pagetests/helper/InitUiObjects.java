package pagetests.helper;


import core.base.Base;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;


public class InitUiObjects {


    public InitUiObjects(){
        PageFactory.initElements(new AppiumFieldDecorator (Base.driver), this);
    }
}
