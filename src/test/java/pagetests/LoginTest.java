package pagetests;


import configs.GetDataFromJson;
import helper.assertionHelper.VerifyControllers;
import helper.wait.WaitHelper;
import helper.wait.WaitStrategy;
import io.appium.java_client.android.Activity;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Test;
import pagetests.helper.AccountManagementUtilities;
import pagetests.helper.Credentials;
import utilities.extentmenager.ExtentsManager;
import java.io.IOException;
import static helper.actions.Commands.*;
import static helper.assertionHelper.VerifyControllers.verifyElementEqualsTo;
import static helper.assertionHelper.VerifyControllers.verifyElementsExistCatch;


public class LoginTest extends AccountManagementUtilities {





    private void navigateToActivity (String appPath, String activityPath) {
        Activity activity = new Activity (appPath, activityPath);
        activity.getAppActivity ();
    }

    private static void validLogIn (String UserName, String PassWord) {
        clickThisOnElement (accountManagementUI.loginPage.enterAccountRl);
        typeToThisTextField (accountManagementUI.loginPage.enterUserInput, UserName);
        typeToThisTextField (accountManagementUI.loginPage.enterPasswordInput, PassWord);
        clickThisOnElement (accountManagementUI.loginPage.enterAccountButton);
    }


    @Test (priority = 1, description = "gui text verifications on login screen")
    public void verifyControllersAndTextsBeforeLoginAction () {
        final String message0 = Credentials.message0 = "לילה טוב,כניסה לחשבונך";
        ExtentsManager.getInstance ().createTest ("Login Pages Test", "Verify Text And Buttons");
        verifyElementsExistCatch (accountManagementUI.loginPage.approveAccsessToMobileLocationAlert);
        clickThisOnElement (accountManagementUI.loginPage.approveAccsessToMobileLocationAlert);
        verifyElementEqualsTo(accountManagementUI.loginPage.enterAccountRl,message0);
        clickThisOnElement (accountManagementUI.loginPage.enterAccountRl);
        verifyElementEqualsTo (accountManagementUI.loginPage.enterUserInput, Credentials.manipulate (true, Credentials.message1));
        verifyElementEqualsTo (accountManagementUI.loginPage.enterPasswordInput, Credentials.message2);
        verifyElementEqualsTo (accountManagementUI.loginPage.enterAccountButton, Credentials.message3);
        verifyElementEqualsTo (accountManagementUI.loginPage.forgotPasswordButton, Credentials.message4);
        clickThisOnElement (accountManagementUI.loginPage.goBackButton);

        try {
            Assert.assertTrue (accountManagementUI.firstAnimationsPage.firstAnimationButton.isDisplayed ());
        } catch (Exception e) {
            if (!NoSuchElementException.class.isAssignableFrom (e.getClass ()))
                throw e;
        }

    }



    @Test(priority = 2,description = "verify proper error message for wrong pass")
    public void negativeLoginWithWrongPassWord(){
        clickThisOnElement(accountManagementUI.loginPage.enterAccountRl);
        typeToThisTextField(accountManagementUI.loginPage.enterUserInput,"dd123456");
        typeToThisTextField(accountManagementUI.loginPage.enterUserInput,GetDataFromJson.PasswordForProdAccountManagement);
        clickThisOnElement(accountManagementUI.loginPage.enterAccountButton);
    }


    @Test(priority = 3,description = "verify proper error message ,when empty password text filed")
    public void negativeLoginWithOutPassWord(){
        clearText(accountManagementUI.loginPage.enterUserInput);
        clearText(accountManagementUI.loginPage.enterPasswordInput);
        typeToThisTextField(accountManagementUI.loginPage.enterUserInput,GetDataFromJson.UserNameForProdAccountManagement);
        typeToThisTextField(accountManagementUI.loginPage.enterUserInput,"df123456");
        clickThisOnElement(accountManagementUI.loginPage.enterAccountButton);
    }


    @Test(priority = 4,description = "verify proper error message ,when empty user text filed")
    public void negativeLoginWithOutUserName()throws IOException{
        clearText(accountManagementUI.loginPage.enterUserInput);
        typeToThisTextField(accountManagementUI.loginPage.enterUserInput,GetDataFromJson.UserNameForProdAccountManagement);
        clickThisOnElement(accountManagementUI.loginPage.enterAccountButton);
        verifyElementsExistCatch(accountManagementUI.loginPage.clickOnAproveWhenUserCrdintitalIsWrong);
        clickThisOnElement(accountManagementUI.loginPage.goBackButton);
    }


    @Test(priority = 5,description = "valid login test",dependsOnMethods = {"negativeLoginWithOutUserName"},expectedExceptions = NoSuchElementException.class)
    public void validLogin()throws IOException{
        if(verifyElementsExistCatch(accountManagementUI.loginPage.textWellcomUser)) {
            validLogIn (GetDataFromJson.UserNameForProdAccountManagement, GetDataFromJson.PasswordForProdAccountManagement);
            verifyElementsExistCatch (accountManagementUI.loginPage.haybetimMispatiyrm);
            clickThisOnElement (accountManagementUI.loginPage.haybetimMispatiyrm);
            verifyElementsExistCatch (accountManagementUI.loginPage.textWellcomUser);
            VerifyControllers.verifyTexts (accountManagementUI.loginPage.textWellcomUser);
            WaitHelper.waitFor (accountManagementUI.loginPage.textWellcomUser, WaitStrategy.PRESENT);
        }else try {
        throw new Exception ("Oh No Shit");
    } catch (Exception e) {
        e.printStackTrace ();
    }





    }

}