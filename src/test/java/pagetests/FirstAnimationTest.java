package pagetests;


import configs.GetDataFromJson;
import helper.actions.Commands;
import helper.assertionHelper.VerifyControllers;
import helper.wait.WaitHelper;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pagetests.helper.AccountManagementUtilities;
import pagetests.helper.Credentials;
import java.util.NoSuchElementException;



public class FirstAnimationTest extends AccountManagementUtilities {



    private Integer TimeOut = 15;
    private Integer PolingTimer = 5;


    static { GetDataFromJson.readFromPropertiesJson();}


    @Test(priority = 1, description = "Check Gui Page One",expectedExceptions = NoSuchElementException.class)
    public void firstPageTest() {
        if(WaitHelper.waitForElementVisible(accountManagementUI.firstAnimationsPage.headerFirstHeadText, TimeOut, PolingTimer)) {
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerFirstHeadText, Credentials.firstHeader);
            VerifyControllers.ifElementCompereTextExists(accountManagementUI.firstAnimationsPage.headerFirstHeadText, Credentials.firstHeader);
            VerifyControllers.verifyTextEquals(accountManagementUI.firstAnimationsPage.headerFirstHeadText, Credentials.firstHeader);
            Commands.clickThisOnElement (accountManagementUI.firstAnimationsPage.firstAnimationButton);
        }
    }


    @Test(priority = 2,description = "Check Gui Page Two",dependsOnMethods = {"firstPageTest"},expectedExceptions = NoSuchElementException.class)
    public void secondsPageTest(){
        if(WaitHelper.waitForElementVisible(accountManagementUI.firstAnimationsPage.headerSecondHeadText,TimeOut,PolingTimer)) {
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerSecondHeadText,Credentials.secondHeader);
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerSecondBodyText,Credentials.secondBody);
            Commands.clickThisOnElement (accountManagementUI.firstAnimationsPage.secondAnimationButton);
        }else try {
            throw new Exception ("Oh No Shit");
        } catch (Exception e) {
            e.printStackTrace ();
        }
    }


    @Test(priority = 3,description = "Check Gui Page Tree",dependsOnMethods = {"secondsPageTest"},expectedExceptions = NoSuchElementException.class)
    public void thirdPageTest(){
        if(WaitHelper.waitForElementVisible(accountManagementUI.firstAnimationsPage.headerThirdHeadText,TimeOut,PolingTimer)) {
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerThirdHeadText,Credentials.manipulate(true,Credentials.thirdHeader));
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerThirdBodyText,Credentials.manipulate(false,Credentials.thirdBody));
        }else try {
            throw new Exception ("Oh No Shit");
        } catch (Exception e) {
            e.printStackTrace ();
        }finally {
            if(WaitHelper.waitForElementVisible(accountManagementUI.firstAnimationsPage.headerThirdHeadText,TimeOut,PolingTimer)) {
                Commands.clickThisOnElement (accountManagementUI.firstAnimationsPage.threeAnimationButton);
            }
        }
    }



    @Test(priority = 4,description = "Check Gui Page Four",dependsOnMethods = {"thirdPageTest"},expectedExceptions = NoSuchElementException.class)
    public void fourPageTest(){
        if(WaitHelper.waitForElementVisible(accountManagementUI.firstAnimationsPage.headerFourHeadText,TimeOut,PolingTimer)) {
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerFourHeadText, Credentials.fourHeader);
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerFourBodyText, Credentials.fourBody);
            Commands.clickThisOnElement (accountManagementUI.firstAnimationsPage.fourAnimationButton);
        }else try {
            throw new Exception ("Oh No Shit");
        } catch (Exception e) {
            e.printStackTrace ();
        }
    }


    @Test(priority = 5,description = "Check Verify Pre Login One",dependsOnMethods = {"fourPageTest"},expectedExceptions = NoSuchElementException.class)
    public void verifyWelcomePage () {
        if(WaitHelper.waitForElementVisible (accountManagementUI.firstAnimationsPage.headerLoginPage, TimeOut, PolingTimer)){
            VerifyControllers.verfyAsserts(true,accountManagementUI.firstAnimationsPage.headerLoginPage, Credentials.loginPage);
    }else try {
        throw new Exception ("Oh No Shit");
        }catch (Exception e) {
            e.printStackTrace ();
        }
    }


    @Test(description = "full animation flow",expectedExceptions = NoSuchElementException.class)
    public void animationFlow(){
        FirstAnimationTest firstAnimationTest = new FirstAnimationTest ();
        firstAnimationTest.firstPageTest();
        firstAnimationTest.secondsPageTest();
        firstAnimationTest.thirdPageTest();
        firstAnimationTest.fourPageTest ();
        firstAnimationTest.verifyWelcomePage();
    }

}
