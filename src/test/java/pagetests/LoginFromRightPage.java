package pagetests;



import configs.GetDataFromJson;
import helper.actions.Commands;
import helper.actions.MyScreenRecorder;
import helper.assertionHelper.VerifyControllers;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pagetests.helper.AccountManagementUtilities;
import utilities.extentmenager.ExtentsManager;
import java.io.IOException;

import static helper.actions.Commands.*;
import static helper.assertionHelper.VerifyControllers.verifyElementPresent;


public class LoginFromRightPage extends AccountManagementUtilities {




    private void moveToRightMenu(){
        try{
            click(accountManagementUI.preLoginPage.rightMenuButton);
        }catch(NoSuchElementException s){
            s.getMessage();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void validLogIn(){
        try {
            MyScreenRecorder.startRecording("valid login");
        } catch (Exception e) {
            e.printStackTrace ();
        }
        typeToThisTextField(accountManagementUI.loginPage.enterUserInput,GetDataFromJson.UserNameForProdAccountManagement);
        typeToThisTextField(accountManagementUI.loginPage.enterPasswordInput,GetDataFromJson.PasswordForProdAccountManagement);
        clickThisOnElement(accountManagementUI.loginPage.enterAccountButton);
        clickThisOnElement(accountManagementUI.loginPage.enterAccountButton);

        try {MyScreenRecorder.stopRecording();
        } catch (Exception e) {
            e.printStackTrace ();
        }
    }


    @BeforeTest
    public void setUp() throws IOException{
        initAppiumSession();
        ExtentsManager.getInstance().createTest("Login Pages From Right Page");
    }
//
//
//    @Test(priority = 1,description = "test login from right page")
//    public void testTransferMoney()throws IOException{
//        ExtentsManager.getInstance ().createTest ("Login Pages From Right Page","Transfer Money");
//        moveToRightMenu();
//        verifyElementsBeforeLoginScreen(loginFromRightPage.transferMoney);
//        loginFromRightPage.clickOnElement(loginFromRightPage.transferMoney);
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.loginHeader);
//        validLogIn ();
//        loginFromRightPage.verifyElementsAfterLoginScreen(loginFromRightPage.headerTitle);
//    }
//
//
//    @Test(priority = 2,description = "test login from right page")
//    public void testDrawFromAtm()throws IOException{
//        moveToRightMenu();
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.drowFromAtm);
//        loginFromRightPage.clickOnElement(loginFromRightPage.drowFromAtm);
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.loginHeader);
//        validLogIn ();
//        loginFromRightPage.verifyElementsAfterLoginScreen(loginFromRightPage.verifyAfteraLogin);
//    }
//
//
//    @Test(priority = 3,description = "test login from right page")
//    public void testPari(){
//        moveToRightMenu();
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.periActions);
//        loginFromRightPage.clickOnElement(loginFromRightPage.periActions);
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.loginHeader);
//        validLogIn ();
//        loginFromRightPage.verifyElementsAfterLoginScreen(loginFromRightPage.periTitle);
//    }
//
//
//    @Test(priority = 4,description = "test login from right page")
//    public void testLoanRequest(){
//        moveToRightMenu();
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.loanRequest);
//        loginFromRightPage.clickOnElement(loginFromRightPage.loanRequest);
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.loginHeader);
//        validLogIn ();
//        loginFromRightPage.verifyElementsAfterLoginScreen(loginFromRightPage.loanRequestLobi);
//    }
//
//
//    @Test(priority = 5,description = "test login from right page")
//    public void testCheckDeposit (){
//        moveToRightMenu();
//        verifyElementsBeforeLoginScreen(loginFromRightPage.cheackDeposit);
//        clickOnElement(loginFromRightPage.cheackDeposit);
//        verifyElementsAfterLoginScreen(loginFromRightPage.cheackDepositButton);
//        loginFromRightPage.clickOnElement(loginFromRightPage.cheackDepositButton);
//        loginFromRightPage.verifyElementsBeforeLoginScreen(loginFromRightPage.loginHeader);
//        validLogIn ();
//        loginFromRightPage.clickOnElement(loginFromRightPage.infoApproveButton);
//        loginFromRightPage.verifyElementsAfterLoginScreen(loginFromRightPage.verifyTitle);
//
//    }


    @Test(priority = 6,description = "test login from right page")
    public void orderMatachTerminal()throws IOException{
        moveToRightMenu();
        verifyElementPresent (accountManagementUI.preLoginPage.OrderMatach);
        clickThisOnElement(accountManagementUI.preLoginPage.OrderMatach);
        verifyElementPresent(accountManagementUI.preLoginPage.loginHeader);
        validLogIn ();
        verifyElementPresent(accountManagementUI.preLoginPage.drowFromAtm);
    }


    @AfterMethod
    public void doAfterEachMethod() throws IOException{
        driver.closeApp();
        initAppiumSession();
    }


}
