package pages;


import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.HowToUseLocators;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagetests.helper.InitUiObjects;

import java.util.ArrayList;

import static io.appium.java_client.pagefactory.LocatorGroupStrategy.ALL_POSSIBLE;


public class LoginPage extends InitUiObjects {

    @HowToUseLocators (androidAutomation = ALL_POSSIBLE , iOSAutomation = ALL_POSSIBLE)
    @AndroidFindBy(id = "com.apps.packageinstaller:id/permission_allow_button")
    @AndroidFindBy(xpath = "todo")
    /* TODO WITH IOS ELEMENTS... */
    public WebElement approveAccsessToMobileLocationAlert;

    @FindBy(id = "com.ideomobile.hapoalim:id/enterAccountRL")
    public WebElement enterAccountRl;

    @FindBy(id = "com.ideomobile.hapoalim:id/sl_userNameInput")
    public WebElement enterUserInput;

    @FindBy(id = "com.ideomobile.hapoalim:id/sl_userPasswordInput")
    public WebElement enterPasswordInput;

    @FindBy(id = "com.ideomobile.hapoalim:id/sl_enterToAccountBtn")
    public WebElement enterAccountButton;

    @FindBy(id = "com.ideomobile.hapoalim:id/wnTxtUserName")
    public WebElement textWellcomUser;

    @FindBy(id = "com.ideomobile.hapoalim:id/smartLoginForgotPassword")
    public WebElement forgotPasswordButton;

    @FindBy(id = "com.ideomobile.hapoalim:id/smartLoginClose")
    public WebElement goBackButton;

    @FindBy(id = "apps:id/button1")
    public WebElement clickOnAproveWhenUserCrdintitalIsWrong;

    @FindBy(id = "com.ideomobile.hapoalim:id/llFBAgree")
    public WebElement haybetimMispatiyrm;



}


