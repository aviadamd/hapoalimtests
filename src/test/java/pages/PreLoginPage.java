package pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagetests.helper.InitUiObjects;


public class PreLoginPage extends InitUiObjects {


    //********this section is for perLogin right Bar**************//
    //transfer money//
    @FindBy(xpath = "//*[@contentDescription='תפריט']")
    public WebElement rightMenuButton;

    //this is the header for all headers in pre login
    @FindBy(xpath = "//*[@resource-id='com.ideomobile.hapoalim:id/wizard_navigation_title']")
    public WebElement headerTitle;

    //transfer money
    @FindBy(xpath = "//*[@text='העברת\n" + "כספים' and @id='actionButton']")
    public WebElement transferMoney;


    @FindBy(xpath = "//*[@resource-id='com.ideomobile.hapoalim:id/smartLoginUserName']")
    public WebElement loginHeader;

    //drow from atm
    @FindBy(xpath = "//*[@text='משיכה\n" + "מבנקט' and @id='actionButton']")
    public WebElement drowFromAtm;

    @FindBy(xpath = "//*[@resource-id='com.ideomobile.hapoalim:id/CWM_btnOther']")
    public WebElement verifyAfteraLogin;

    //peri
    @FindBy(xpath = "//*[@text=concat('פעולות\n" + "בפר', '\"', 'י') and @id='actionButton']")
    public WebElement periActions;

    @FindBy(id = "com.ideomobile.hapoalim:id/DRM_txtTitle")
    public WebElement periTitle;
    //loan
    @FindBy(xpath = "//*[@text='בקשת\n" + "הלוואה' and @id='actionButton']")
    public WebElement loanRequest;

    @FindBy(id = "com.ideomobile.hapoalim:id/header_text")
    public WebElement loanRequestLobi;

    //cheack depost
    @FindBy(xpath = "//*[@text='הפקדת שיקים']")
    public WebElement cheackDeposit;

    @FindBy(id = "com.ideomobile.hapoalim:id/CN_btnScanCheck")
    public WebElement cheackDepositButton;

    @FindBy(id = "com.ideomobile.hapoalim:id/SCML_btnNext")
    public WebElement infoApproveButton;

    @FindBy(id = "com.ideomobile.hapoalim:id/wizard_navigation_title")
    public WebElement verifyTitle;

    @FindBy(xpath = "//*[@text=concat('הזמנת מט', '\"', 'ח בטרמינל')]")
    public WebElement OrderMatach;

    @FindBy(id = "com.ideomobile.hapoalim:id/wizard_navigation_title")
    public WebElement OrderMatachTitleAfterLogin;

    @FindBy(id = "com.ideomobile.hapoalim:id/enterAccountRL")
    public WebElement enterAccountRl;

    @FindBy(id = "com.ideomobile.hapoalim:id/sl_userNameInput")
    public WebElement enterUserInput;

    @FindBy(id = "com.ideomobile.hapoalim:id/sl_userPasswordInput")
    public WebElement enterPasswordInput;

    @FindBy(id = "com.ideomobile.hapoalim:id/sl_enterToAccountBtn")
    public WebElement enterAccountButton;

}
