package pages;



import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pagetests.helper.InitUiObjects;
import java.util.Optional;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;



public class FirstAnimationsPage extends InitUiObjects {


    WebDriver driver;





       @FindBy (id = "com.ideomobile.hapoalim:id/btn_next")
       public WebElement firstAnimationButton;

       @FindBy (id = "com.ideomobile.hapoalim:id/scene4_header")
       public WebElement headerFirstHeadText;

       @FindBy (id = "com.ideomobile.hapoalim:id/btn_next")
       public WebElement secondAnimationButton;

       @FindBy (id = "com.ideomobile.hapoalim:id/scene1_header")
       public WebElement headerSecondHeadText;

       @FindBy (id = "com.ideomobile.hapoalim:id/scene1_text")
       public WebElement headerSecondBodyText;

       @FindBy (id = "com.ideomobile.hapoalim:id/btn_next")
       public WebElement threeAnimationButton;

       @FindBy (id = " com.ideomobile.hapoalim:id/scene2_header")
       public WebElement headerThirdHeadText;

       @FindBy (id = "com.ideomobile.hapoalim:id/scene2_text")
       public WebElement headerThirdBodyText;

       @FindBy (id = "com.ideomobile.hapoalim:id/btn_next")
       public WebElement fourAnimationButton;

       @FindBy (id = "com.ideomobile.hapoalim:id/scene3_header")
       public WebElement headerFourHeadText;

       @FindBy (id = "com.ideomobile.hapoalim:id/scene3_text")
       public WebElement headerFourBodyText;

       @FindBy (id = "com.ideomobile.hapoalim:id/scene3_text")
       public WebElement headerLoginPage;


    @Test
    public void givenNonNull_whenCreatesNonNullable_thenCorrect() {

        Optional<String> opt = Optional.of(firstAnimationButton.getText());
        assertTrue(opt.isPresent());
        Optional<Boolean> optionalBoolean = Optional.of(firstAnimationButton.isDisplayed());
        assertTrue(optionalBoolean.isPresent());
    }

    @Test
    public void givenOptional_whenIfPresentWorks_thenCorrect() {
        Optional<WebElement> optionalWebElement = Optional.of(firstAnimationButton);
        optionalWebElement.ifPresent(element -> element.isDisplayed());
        optionalWebElement.ifPresent(WebElement::isDisplayed);
    }

    @Test
    public void whenOrElseWorks_thenCorrect() {
        String nullName = null;
        String name = Optional.ofNullable(nullName).orElse("john");
        assertEquals("john", name);
    }

       /**
        * this can be implement if there is answer code from the server
        * or parse json answer from the server ...
        * @param element
        */
       public void verifyTextFromPage (WebElement element){
           switch (element.getText ()) {
               case "כן":
                   //TODO
                   System.out.println ("The Page Cell Is Empty ... ");
                   break;
               case "no":
                   //TODO
                   System.out.println ("The Page Cell Is With ...");
                   break;
               case "":
                   //TODO
                   System.out.println ("todo");
                   break;
           }
       }

}
