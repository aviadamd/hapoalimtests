package activityTests;//
//package activitytests;
//
//import io.appium.java_client.NoSuchContextException;
//import io.appium.java_client.apps.Activity;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import static core.listeners.errorCollector.ErrorCollector.assertEquals;
//
//
//public class AndroidContextTest extends BaseAndroidTest {
//
//    @BeforeClass
//    public static void beforeClass2() throws Exception
//    {
//        Activity activity = new Activity("io.appium.apps.apis", ".view.WebView1");
//        driver.startActivity(activity);
//        Thread.sleep(20000);
//    }
//
//    @Test
//    public void testGetContext() {
//        assertEquals("NATIVE_APP", driver.getContext());
//    }
//
//    @Test public void testGetContextHandles() {
//        assertEquals(driver.getContextHandles().size(), 2);
//    }
//
//    @Test public void testSwitchContext() {
//        driver.getContextHandles();
//        driver.context("WEBVIEW_io.appium.apps.apis");
//        assertEquals(driver.getContext(), "WEBVIEW_io.appium.apps.apis");
//        driver.context("NATIVE_APP");
//        assertEquals(driver.getContext(), "NATIVE_APP");
//    }
//
//    @Test
//    public void testContextError()
//    {
//        driver.context("Planet of the Ape-ium");
//        assertEquals("Planet of the Ape-ium", driver.getContext());
//    }
//
//}
