//package activityTests;
//
//
// import core.base.Base;
// import io.appium.java_client.apps.AndroidDriver;
// import io.appium.java_client.apps.AndroidElement;
// import io.appium.java_client.apps.StartsActivity;
// import io.appium.java_client.remote.AutomationName;
// import io.appium.java_client.remote.MobileCapabilityType;
// import io.appium.java_client.service.local.AppiumDriverLocalService;
// import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
// import org.openqa.selenium.remote.DesiredCapabilities;
// import org.testng.annotations.AfterClass;
// import org.testng.annotations.BeforeClass;
//
// import java.io.File;
//
// public class BaseAndroidTest extends Base
// {
//    public static final String APP_ID = "io.appium.apps.apis";
//    private static AppiumDriverLocalService service;
//    protected static AndroidDriver<AndroidElement> driver;
//    private StartsActivity startsActivity;
//
//    /**
//     * initialization.
//     */
//    @BeforeClass
//    public static void beforeClass() {
//        service = AppiumDriverLocalService.buildDefaultService();
//        service.start();
//
//        if (service == null || !service.isRunning()) {
//            throw new AppiumServerHasNotBeenStartedLocallyException(
//                    "An appium server node is not started!");
//        }
//        File appDir = new File("src/test/java/io/appium/java_client");
//        File app = new File(appDir, "ApiDemos-debug.apk");
//        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
//        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
//        capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
//        driver = new AndroidDriver<>(service.getUrl(), capabilities);
//    }
//
//    /**
//     * finishing.
//     */
//    @AfterClass
//    public static void afterClass() {
//        if (driver != null) {
//            driver.quit();
//        }
//        if (service != null) {
//            service.stop();
//        }
//    }
//}
