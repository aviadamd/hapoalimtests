package activityTests;//
//package activitytests;
//
//import io.appium.java_client.apps.connection.ConnectionState;
//import io.appium.java_client.apps.connection.ConnectionStateBuilder;
//import org.testng.annotations.Test;
//
//import static core.listeners.errorCollector.ErrorCollector.ASSERT_FALSE;
//import static core.listeners.errorCollector.ErrorCollector.assertTrue;
//
//
//
//public class AndroidConnectionTest extends BaseAndroidTest {
//
//    @Test
//    public void test1() {
//        ConnectionState state = driver.setConnection(new ConnectionStateBuilder()
//                .withWiFiEnabled()
//                .build());
//        assertTrue(state.isWiFiEnabled());
//    }
//
//    @Test
//    public void test2() {
//        ConnectionState state = driver.setConnection(new ConnectionStateBuilder()
//                .withAirplaneModeDisabled()
//                .build());
//        ASSERT_FALSE(state.isAirplaneModeEnabled());
//        ASSERT_FALSE(state.isWiFiEnabled());
//        ASSERT_FALSE(state.isDataEnabled());
//        state = driver.setConnection(new ConnectionStateBuilder(state)
//                .withAirplaneModeEnabled()
//                .build());
//        assertTrue(state.isAirplaneModeEnabled());
//    }
//
//    @Test
//    public void test3() {
//        ConnectionState state = driver.setConnection(
//                new ConnectionStateBuilder(driver.getConnection())
//                        .withAirplaneModeDisabled()
//                        .withWiFiEnabled()
//                        .withDataEnabled()
//                        .build());
//        ASSERT_FALSE(state.isAirplaneModeEnabled());
//        assertTrue(state.isWiFiEnabled());
//        assertTrue(state.isDataEnabled());
//    }
//}
