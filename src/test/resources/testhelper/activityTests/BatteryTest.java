//package activityTests;
//
// import io.appium.java_client.apps.AndroidBatteryInfo;
// import org.testng.annotations.Test;
//
// import static org.hamcrest.CoreMatchers.is;
// import static org.hamcrest.CoreMatchers.not;
// import static org.hamcrest.MatcherAssert.assertThat;
// import static org.hamcrest.Matchers.greaterThan;
//
//
//public class BatteryTest extends BaseAndroidTest {
//
//    @Test
//    public void veryGettingBatteryInformation() {
//        final AndroidBatteryInfo batteryInfo = driver.getBatteryInfo();
//        assertThat(batteryInfo.getLevel(), is(greaterThan(0.0)));
//        assertThat(batteryInfo.getState(), is(not(AndroidBatteryInfo.BatteryState.UNKNOWN)));
//
//    }
//}
