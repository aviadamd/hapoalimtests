package activityTests;//package activitytests;
//
//import io.appium.java_client.apps.Activity;
//import io.appium.java_client.apps.nativekey.AndroidKey;
//import io.appium.java_client.apps.nativekey.KeyEvent;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//
//import static core.listeners.errorCollector.ErrorCollector.assertEquals;
//
//public class AndroidActivityTest extends BaseAndroidTest
//{
//    @BeforeTest
//    public void setUp() {
//        Activity activity = new Activity("io.appium.apps.apis", ".ApiDemos");
//        driver.startActivity(activity);
//    }
//
//    @Test
//    public void startActivityInThisAppTestCase()
//    {
//        Activity activity = new Activity("io.appium.apps.apis",
//                ".accessibility.AccessibilityNodeProviderActivity");
//        driver.startActivity(activity);
//        assertEquals(driver.currentActivity(),
//                ".accessibility.AccessibilityNodeProviderActivity");
//    }
//
//    @Test
//    public void startActivityWithWaitingAppTestCase()
//    {
//        final Activity activity = new Activity("io.appium.apps.apis",
//                ".accessibility.AccessibilityNodeProviderActivity")
//                .setAppWaitPackage("io.appium.apps.apis")
//                .setAppWaitActivity(".accessibility.AccessibilityNodeProviderActivity");
//        driver.startActivity(activity);
//        assertEquals(driver.currentActivity(),
//                ".accessibility.AccessibilityNodeProviderActivity");
//    }
//
//    @Test
//    public void startActivityInNewAppTestCase()
//    {
//        Activity activity = new Activity("com.apps.settings", ".Settings");
//        driver.startActivity(activity);
//        assertEquals(driver.currentActivity(), ".Settings");
//        driver.pressKey(new KeyEvent(AndroidKey.BACK));
//        assertEquals(driver.currentActivity(), ".ApiDemos");
//    }
//
//    @Test
//    public void startActivityInNewAppTestCaseWithoutClosingApp()
//    {
//        Activity activity = new Activity("io.appium.apps.apis",
//                ".accessibility.AccessibilityNodeProviderActivity");
//        driver.startActivity(activity);
//        assertEquals(driver.currentActivity(), ".accessibility.AccessibilityNodeProviderActivity");
//
//        Activity newActivity = new Activity("com.apps.settings", ".Settings")
//                .setAppWaitPackage("com.apps.settings")
//                .setAppWaitActivity(".Settings")
//                .setStopApp(false);
//        driver.startActivity(newActivity);
//        assertEquals(driver.currentActivity(), ".Settings");
//        driver.pressKey(new KeyEvent(AndroidKey.BACK));
//        assertEquals(driver.currentActivity(), ".accessibility.AccessibilityNodeProviderActivity");
//    }
//}
