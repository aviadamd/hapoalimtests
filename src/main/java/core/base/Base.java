package core.base;

import configs.EnvironmentConfigKeys;
import configs.GetDataFromJson;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.TestNGException;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;
import utilities.extentmenager.ExtentsManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ServiceConfigurationError;
import java.util.concurrent.TimeUnit;


public class Base extends ExtentsManager {



    private ExceptionInInitializerError exceptionInInitializerError = new ExceptionInInitializerError ();
    public static AndroidDriver<AndroidElement> driver;
    private static DesiredCapabilities dc = new DesiredCapabilities ();
    private static AppiumDriver serverDriver;
    private Log logger = new Log();
    private static AppiumDriver appiumDriver = (AppiumDriver) driver;
    private static AppiumDriverLocalService service;




    public Base(){
        try {
            logger.writeToLogAndConsole (LogLevel.INFO, "Init Android Driver");
            initAppiumSession();
        }catch(WebDriverException e) {
            logger.writeToLogAndConsole(LogLevel.ERROR,e.getMessage());
        }catch (NullPointerException n){
            logger.writeToLogAndConsole (LogLevel.ERROR,n.getCause());
        } catch (IOException e) {
            logger.writeToLogAndConsole (LogLevel.ERROR,e.getStackTrace());
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    /**
     * this is for emulator only
     * @throws Exception
     */
    public void logPerformanceData () throws Exception {
        List<List<Object>> performanceData;
        performanceData = driver.getPerformanceData
                ("com.ideomobile.hapoalim", "cpuinfo", 5);
    }

    private boolean disableAndroidWatchers(boolean isCapabilityForDisableAndroidWatchers) {
        try {
            if (isCapabilityForDisableAndroidWatchers) {
                dc.setCapability (AndroidMobileCapabilityType.DISABLE_ANDROID_WATCHERS, true);
                return true;
            } else if (!isCapabilityForDisableAndroidWatchers) {
                dc.setCapability (AndroidMobileCapabilityType.DISABLE_ANDROID_WATCHERS, false);
                return false;
            } else {
                exceptionInInitializerError.getCause ();
            }
            return true;
        } catch (Exception e) {
            logger.writeToLogAndConsole (LogLevel.DEBUG, e.getMessage ());
        }
        return true;
    }

    private boolean setAcceptInsecureCerts(boolean isSetAcceptInsecureCerts){
        try {
            if (isSetAcceptInsecureCerts){
                dc.setAcceptInsecureCerts (true);
            }else if (isSetAcceptInsecureCerts){
                dc.setAcceptInsecureCerts (false);
            }else {
                System.out.println ("");
            }
        }catch (ServiceConfigurationError e){
            e.getMessage ();
        }
        return true;
    }

    private void isSkipUnlock(boolean writeIfWantToSkipUnlock){
        try {
            if (writeIfWantToSkipUnlock) {
                dc.setCapability ("skipUnlock", true);
            } else if (!writeIfWantToSkipUnlock) {
                dc.setCapability ("skipUnlock", false);
            } else {
                logger.writeToLogAndConsole (LogLevel.DEBUG, "");
            }
        }catch (ServiceConfigurationError e){
            e.getMessage ();
        }
    }

    /**
     * hard coded
     * will use for un
     * reset every time
     * when start session
     * @throws Exception
     */
    public void initAppiumSession() throws IOException {
        dc.setCapability(MobileCapabilityType.UDID, "6beb6497d440");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.ideomobile.hapoalim");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".ActivityLauncher");
        dc.setCapability (MobileCapabilityType.DEVICE_NAME,"Redmi4X");
        driver = new AndroidDriver<> (new URL("http://localhost:4723/wd/hub"), dc);
        driver.manage ().timeouts().pageLoadTimeout (20, TimeUnit.SECONDS);
    }

    /**
     * use with params
     * from json file
     * will use for un
     * reset every time
     * when start session
     * @throws Exception
     */
    public void initSession()  {
        dc.setCapability (MobileCapabilityType.UDID,GetDataFromJson.Udid);
        dc.setCapability (AndroidMobileCapabilityType.APP_PACKAGE, GetDataFromJson.AppPackage);
        dc.setCapability (AndroidMobileCapabilityType.APP_ACTIVITY, GetDataFromJson.AppActivity);
        dc.setCapability (MobileCapabilityType.AUTOMATION_NAME, GetDataFromJson.AutomationName);
        if(driver == null){
            try {
                driver = new AndroidDriver<> (new URL (GetDataFromJson.Url), dc);
            }catch (Exception urlChecker){
                urlChecker.getMessage ();
            }
            driver.manage ().timeouts ().pageLoadTimeout (20, TimeUnit.SECONDS);
        }else {
            logger.writeToLogAndConsole (LogLevel.ERROR, "Cannot Init Session With Android Driver");

        }
    }

    /**
     * this method As Been Called from CapabilityInit
     * will launch the appium server pragmatically
     * @return - Returns driver instance
     */
    private static AppiumDriver initAndroidDriverServer(){
        if (serverDriver != null) {
            return serverDriver;
        } else {
            service = AppiumDriverLocalService
                    .buildService (new AppiumServiceBuilder()
                            .withIPAddress ("127.0.0.1")
                            .usingAnyFreePort()
                            .usingDriverExecutable(new File(EnvironmentConfigKeys.NodeJs))
                            .withAppiumJS(new File(EnvironmentConfigKeys.NodeModule)));
            String service_url = service.getUrl().toString();
            System.out.println("start " + service_url);
            service.start ();
        }
        return serverDriver;
    }
}



