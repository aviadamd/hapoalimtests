package core.base;

import configs.EnvironmentConfigKeys;
import org.apache.commons.io.FileUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.*;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import utilities.customlogger.Log;
import utilities.extentmenager.ExtentManagerFiles;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Date;
import static org.apache.commons.io.FileUtils.copyFile;


public class BaseUtility extends Base
{
    public Log logger = new Log();
    public static Date d = new Date ();
    public static String screenshotName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";



    public void createDirectory(String directoryPath){
        deleteDirectory(directoryPath);
        File file = new File(directoryPath);
        if (!file.exists()) {
            if (file.mkdirs()) {
                System.out.println("BaseLine Image Directory is created!");
            } else {
                System.out.println("Failed to create BaseLine image directory!");
            }
        }
    }


    private void deleteDirectory(String path){
        File file = new File(path);
        if (file.isDirectory()){
            System.out.println("Deleting Directory :" + path);
            try{
                FileUtils.deleteDirectory(new File(path)); //deletes the whole folder
            }catch(IOException e){
                e.printStackTrace();
            }
        }else{
            System.out.println("Deleting File :" + path);
            file.delete();
        }
    }



    public String getParentDirectoryFromFile(File file){
        return file.getParent();
    }



    public static void captureScreenshot() throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        Date d = new Date();
        screenshotName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";
        copyFile(scrFile,new File(ExtentManagerFiles.reportFileName + screenshotName ));

    }



    public static String takeScreenShot()throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs (OutputType.FILE);
        Date date = new Date ();
        String ScreenShot = date.toString ().replace (":","_").replace ("","_") + ".jpg";
        copyFile(scrFile,new File(ExtentManagerFiles.reportFileName + ScreenShot));
        return ScreenShot;
    }



    public void saveAsImage(byte[] imageAsByteArray, String name) {
        InputStream in = new ByteArrayInputStream (imageAsByteArray);
        BufferedImage bImageFromConvert = null;
        File file;
        try {
            file = new File ("./Images/" + name);
            bImageFromConvert = ImageIO.read (new ByteArrayInputStream (imageAsByteArray));
            ImageIO.write (bImageFromConvert, "jpg", file);
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }


    public static void capture(String filePath) {
        try {
            BufferedImage screencapture = new Robot ().createScreenCapture(
                    new java.awt.Rectangle (Toolkit.getDefaultToolkit().getScreenSize()));
            File file = new File(filePath);
            ImageIO.write(screencapture, "jpg", file);

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void capturePNG(String filePath) {
        try {
            BufferedImage screencapture = new Robot().createScreenCapture(
                    new java.awt.Rectangle (Toolkit.getDefaultToolkit().getScreenSize()));
            File file = new File(filePath);
            ImageIO.write(screencapture, "png", file);

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] capturePNG() {
        byte[] out = null;
        try {
            BufferedImage screencapture = new Robot().createScreenCapture(
                    new java.awt.Rectangle (Toolkit.getDefaultToolkit().getScreenSize()));

            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ImageIO.write(screencapture, "png", bo);
            out = bo.toByteArray();
            bo.close();

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }

    public static byte[] capture() {
        byte[] out = null;
        try {
            BufferedImage screencapture = new Robot ().createScreenCapture (
                    new Rectangle (Toolkit.getDefaultToolkit ().getScreenSize ()));
            ByteArrayOutputStream bo = new ByteArrayOutputStream ();
            ImageIO.write (screencapture, "jpg", bo);
            out = bo.toByteArray ();
            bo.close ();
        } catch (AWTException e) {
            e.printStackTrace ();
        } catch (IOException e) {
            e.printStackTrace ();
        }
        return out;
    }

    public static Object jsonGetData(String nodeName){
        JSONParser parser = new JSONParser();
        Reader reader = null;
        try {
            reader = new FileReader (EnvironmentConfigKeys.JSON_CONSTANTS);
        } catch (FileNotFoundException e) {
            e.printStackTrace ();
        }
        Object jsonObj = null;
        try {
            jsonObj = parser.parse(reader);
        } catch (IOException e) {
            e.printStackTrace ();
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return jsonObj;
    }

}


