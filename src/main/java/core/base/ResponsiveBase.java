package core.base;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class ResponsiveBase{

    public AndroidDriver getAndroidChromeDriver(String deviceName, String appiumServerURL, Proxy proxy)
            throws MalformedURLException {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("automationName", "Appium");
        dc.setCapability("platformName", "Android");
        dc.setCapability("browserName", "Chrome");
        dc.setCapability("deviceName", deviceName);
        if(proxy != null)
            dc.setCapability(CapabilityType.PROXY,proxy);
        //Initiate WebDriver
        return new AndroidDriver(new URL(appiumServerURL), dc);
    }

    public Platform getPlatform(String platformName) {

        String osName = platformName.toUpperCase();
        if(osName.equals("WIN8.1"))
            return Platform.WIN8_1;
        else if (osName.equals("WIN8"))
            return Platform.WIN8;
        else if (osName.equals("ANDROID"))
            return Platform.ANDROID;
        else if (osName.equals("LINUX"))
            return Platform.LINUX;
        else if (osName.equals("MAC"))
            return Platform.MAC;
        else if (osName.equals("WIN"))
            return Platform.WINDOWS;
        else
            return Platform.ANY;
    }
}
