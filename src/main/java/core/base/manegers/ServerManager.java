package core.base.manegers;

import core.base.Base;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;

import java.io.*;
import java.util.Scanner;



public class ServerManager {

    private static String OS;
    private static String ANDROID_HOME;
    private static Log logger = new Log ();
    private static AppiumDriver appiumDriver = (AppiumDriver)Base.driver;



    public static void getOsPlatform(Object o){
        Base base = new Base ();
        if (appiumDriver.getPlatformName().equals ("iOS")){
            try {
                base.initAppiumSession ();
            } catch (IOException e) {
                e.printStackTrace ();
            }
            logger.writeToLogAndConsole (LogLevel.DEBUG,appiumDriver.getPlatformName ());
        }
        if (appiumDriver.getPlatformName ().equals ("ANDROID")){
            try {
                base.initAppiumSession();
            } catch (IOException e) {
                e.printStackTrace ();
            }
            logger.writeToLogAndConsole (LogLevel.DEBUG,appiumDriver.getPlatformName ());
        }else {
            throw new RuntimeException("Error");
        }
    }


    public static String getAndroidHome(){
        if(ANDROID_HOME == null) {
            ANDROID_HOME = System.getenv("ANDROID_HOME");
            logger.writeToLog(LogLevel.DEBUG,ANDROID_HOME);
            if(ANDROID_HOME == null) throw new RuntimeException
                    ("Failed to find ANDROID_HOME, make sure the environment variable is set");
        }
        return ANDROID_HOME;
    }

    @Test
    public void getThis(){
        getAndroidHome ();
        getWorkingDir ();
        getOS ();
    }

    public static String getOS(){
        if(OS == null) OS = System.getenv("os.name");
        logger.writeToLogAndConsole (LogLevel.DEBUG,OS);
        return OS;
    }

    public static boolean isWindows(){
        return getOS().startsWith("Windows");
    }

    public static boolean isMac(){
        return getOS().startsWith("Mac");
    }

    public void verifyOs() {

        try {
            if (isMac ()) {
                getOS ();
            }
            if (isWindows ()) {
                getOS ();
            }
        } catch (Exception e) {
            e.getMessage ();
        }
    }

    public static String runCommand(String command){
        String output;
        output = null;
        try{
            Scanner scanner = new Scanner(Runtime.getRuntime().exec(command).getInputStream()).useDelimiter("\\A");
            if(scanner.hasNext()) output = scanner.next();
        }catch (IOException e){
            throw new RuntimeException(e.getMessage());
        }
        return output;
    }

    public static String getWorkingDir()
    {
        System.out.println (System.getProperty("user.dir"));
        return System.getProperty("user.dir");
    }

    public static String read(File file){
        StringBuilder output = new StringBuilder();
        try{
            String line;
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) output.append(line+"\n");
            bufferedReader.close();
        }catch (IOException error){
            error.printStackTrace();
        }
        return output.toString();
    }

    public static void write(File file, String content)
    {
        try
            (Writer writer = new BufferedWriter
                    (new OutputStreamWriter (new FileOutputStream (file), "utf-8")))
            {
                writer.write (content);
            }catch(IOException error){
                error.printStackTrace ();
        }
    }
}