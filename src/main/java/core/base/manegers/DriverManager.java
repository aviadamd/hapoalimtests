package core.base.manegers;

import configs.EnvironmentConfigKeys;
import core.base.Base;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;
import utilities.timer.Timer;
import configs.ResourceHelper;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.service.DriverService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

public class DriverManager {

    private static String APPIUM_HOME = "APPIUM_HOME";
    private static String nodeJS = System.getenv(APPIUM_HOME)+"/node.exe";
    private static String appiumJS = System.getenv(APPIUM_HOME)+"/node_modules/appium/bin/appium.js";
    private static DriverService service;
    private static String deviceID;
    public static WebDriver driver;
    private static HashMap<String, URL> hosts;
    private static String unlockPackage = "io.appium.unlock";
    public static Properties prop = new Properties();
    static InputStream input = null;
    private static Log logger = new Log ();
    public Base base = new Base();


    private static DesiredCapabilities getCaps(String deviceID)
    {
        logger.writeToLog(LogLevel.DEBUG,"Creating driver caps for device: "+deviceID);
        DesiredCapabilities caps = new DesiredCapabilities ();
            caps.setCapability("deviceName", deviceID);
            caps.setCapability("platformName", "Android");
        return caps;
    }

    private static URL host(String deviceID) throws MalformedURLException {
        if(hosts == null){
            hosts = new HashMap<String, URL>();
            hosts.put("device", new URL("http://127.0.0.1:4723/wd/hub"));
        }return hosts.get(deviceID);
    }

    private static ArrayList<String> getAvailableDevices()
    {
        logger.writeToConsole(LogLevel.DEBUG,"Checking for Available Devices");
        ArrayList<String> avaiableDevices = new ArrayList<String>();
        ArrayList connectedDevices = ADB.getConnectedDevices();

        for(Object connectedDevice: connectedDevices){
            String device = connectedDevice.toString();
            ArrayList apps = new ADB(device).getInstalledPackages();
            if(!apps.contains(unlockPackage)){
                if(useDevice(deviceID)) avaiableDevices.add(device);
                else logger.writeToLogAndConsole(LogLevel.DEBUG_ERROR,"Device: "+deviceID+" is being used by another JVM");
            }
            else logger.writeToLogAndConsole(LogLevel.DEBUG,
                    "Device: "+device+" has "+unlockPackage+" installed, assuming it is under testing");
        }
        if (avaiableDevices.size() == 0) throw new RuntimeException
                ("Not a single device is available for testing at this time");
        return avaiableDevices;
    }


    private static DriverService createService() throws MalformedURLException
    {
        service = new AppiumServiceBuilder ()
                .usingDriverExecutable(new File(nodeJS))
                .withAppiumJS(new File(appiumJS))
                .withIPAddress(host(deviceID).toString().split
                        (":")[1].replace("//", ""))
                .usingPort (Integer.parseInt(host(deviceID)
                        .toString().split(":")[2].replace("/wd/hub","")))
                .withArgument(Arg.TIMEOUT, "120")
                .withArgument (Arg.LOCAL_TIME_ZONE,"info")
                .withArgument (Arg.TIME_STAMP, "dd-MMM-yyyy")
                .withArgument(Arg.LOG_LEVEL, "warn")
                .build();
        return service;
    }

    public static void createDriver()  {
        ArrayList<String> devices = getAvailableDevices();
        for(String device : devices) {
            ADB adb = null;
            try {
                deviceID = device;
                if (useDevice (deviceID)) {
                    queueUp ();
                    gracePeriod ();
                    logger.writeToLog(LogLevel.DEBUG,"Trying to create new Driver for device: " + device);
                    createService ().start ();
                    driver = new AndroidDriver (host (device), getCaps (device));
                    adb = new ADB (device);
                    leaveQueue ();
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace ();
                //Ignore and try next device
            } finally {
                logger.writeToLog(LogLevel.INFO,adb.getAndroidVersion());
                logger.writeToLog(LogLevel.INFO,adb.getInstalledPackages());
            }
        }
    }


    public static WebDriver stopDriver()
    {
        if(driver != null)
        {
            logger.writeToLog (LogLevel.DEBUG,"Stop Android Driver");
            driver.quit();
            ADB adb = null;
            adb.uninstallApp(unlockPackage);
            service.stop();
        }else logger.writeToLog (LogLevel.DEBUG,"Android Driver is not initialized, nothing to Stop");
        return driver;
    }

    private static void queueUp() {
        try {
            logger.writeToLog(LogLevel.DEBUG,"Queueing Up: " + deviceID);
            JSONObject json = new JSONObject ();
            json.put ("queued_at", Timer.getTimeStamp ());
            JSONObject jsonQueue = ResourceHelper.getQueue ();
            jsonQueue.put (deviceID, json);
            logger.writeToLog (LogLevel.INFO,"JSON Queue: " + jsonQueue);
            ServerManager.write (new File (ResourceHelper.QUEUE), jsonQueue.toString ());
        } catch (IOException | ParseException e) {
            throw new RuntimeException (e);
        }
    }

    private static boolean useDevice(String deviceID) {
        try {
            JSONObject json = ResourceHelper.getQueue();
            if(json.containsKey(deviceID)){
                JSONObject deviceJson = (JSONObject) json.get(deviceID);
                long time = (long) deviceJson.get("queued_at");
                int diff = Timer.getDifference(time, Timer.getTimeStamp());
                if(diff >= 30) return true;
                else return false;
            } else return true;
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private static void gracePeriod()
    {
        int waitTime = 0;
        try {
            JSONObject json = ResourceHelper.getQueue();
            Set keys = json.keySet();

            JSONObject ourDeviceJson = (JSONObject) json.get(deviceID);
            json.remove(deviceID);
            long weQueuedAt = (long) ourDeviceJson.get("queued_at");

            for(Object key : keys){
                JSONObject deviceJson = (JSONObject) json.get(key);
                long theyQueuedAt = (long) deviceJson.get("queued_at");
                //If we did not queue first we need to wait for the other
                // device to initialize driver so there is no collision
                if(weQueuedAt > theyQueuedAt) {
                    //But only if device queued first and recently,
                    // otherwise we can assume device was already initialized or no longer being used
                    int diff = Timer.getDifference(theyQueuedAt,Timer.getTimeStamp());
                    if(diff < 50){
                        logger.writeToLog(LogLevel.DEBUG,"Device: "+key+" queued first, I will need to give it extra time to initialize");
                        waitTime += 15;
                    }
                }
            }
            try {Thread.sleep(waitTime);} catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        } catch (IOException | ParseException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void leaveQueue(){
        try {
            JSONObject jsonQueue = ResourceHelper.getQueue();
            jsonQueue.remove(deviceID);
            ServerManager.write(new File(ResourceHelper.QUEUE), jsonQueue.toString());
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public WebDriver getDriver () throws Exception
    {
        input = new FileInputStream(EnvironmentConfigKeys.JSON_CONSTANTS);
        prop.load(input);
        if (prop.getProperty("ios").equals("ios")) {
            base.initSession();
        } else {
            if (prop.getProperty ("android").equals ("android")) {
                base.initSession();
            }
        }
        return driver;
    }

}
