package core.base.manegers;

import io.appium.java_client.service.local.flags.ServerArgument;

/**
 * When Using Appium Server
 * You Can Call This Server Arguments
 */
public enum Arg implements ServerArgument {

    TIMEOUT("--command-timeout"),
    LOCAL_TIME_ZONE("--local-timezone"),
    LOG_LEVEL("--log-level"),
    TIME_STAMP("--log-timestamp");

    private final String Arg;

    Arg(String Arg) {
        this.Arg = Arg;
    }

    @Override
    public String getArgument() {

        return Arg;
    }

}
