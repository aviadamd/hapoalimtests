package core.listeners.futureCustomExceptions;




public class MyErrors extends Exception {


    /**
     * The MyErrors wraps all checked standard Java exception and enriches them with a custom error code.
     * You can use this code to retrieve localized error messages and to link to our online documentation.
     */


    private final MyErrorTypes code;



    public MyErrors(MyErrorTypes code) {
        super();
        this.code = code;
    }

    public MyErrors(String message, MyErrorTypes code) {
        super(message);
        this.code = code;
    }

    public MyErrors(String message, Throwable cause, MyErrorTypes code) {
        super(message, cause);
        this.code = code;
    }

    public MyErrors(Throwable cause, MyErrorTypes code) {
        super(cause);
        this.code = code;
    }


    public MyErrorTypes getCode() {
        return this.code;
    }




}