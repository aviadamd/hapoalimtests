package core.listeners.futureCustomExceptions;

public enum MyErrorTypes {



    GENERIC_FAILURE("0"),
    WRITE_FAILURE("1"),
    FLUSH_FAILURE("2"),
    CLOSE_FAILURE("3"),
    FILE_OPEN_FAILURE("4"),
    MISSING_LAYOUT("5"),
    ADDRESS_PARSE_FAILURE("6");


    private final String level;


    MyErrorTypes(final String level){
        this.level = level;
    }


    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString(){
        return this.level;
    }
}
