package core.listeners.errorCollector;


import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class ErrorCollector {




	private static Map<ITestResult, List<Throwable>> verificationFailuresMap = new HashMap<>();
    private static SoftAssert softAssert = null;
    private static<K>void assertTrue(K condition){Assert.assertTrue((Boolean)condition);}
    private static<K>void assertFalse(K condition){Assert.assertFalse((Boolean)condition);}
    private static<K,V>void assertEquals(K actual,V expected){Assert.assertEquals(actual,expected);}
	private static<K,V>void assertNotEquals(K actual,V expected){Assert.assertNotEquals(actual,expected);}
	private static<K>void assertNull(K type){Assert.assertNull(type);}
	private static<K>void assertNotNull(K type){Assert.assertNotNull(type);}
	private static<K,V>void assertSame(K type,V type1){Assert.assertSame (type,type1);}
	private static<K,V>void assertNotSame(K type,V type1){Assert.assertNotSame(type,type1);}



	public static List<Throwable> getVerificationFailures(){
		List<Throwable> verificationFailures = verificationFailuresMap.get(Reporter.getCurrentTestResult());
		return verificationFailures == null ? new ArrayList <> () : verificationFailures;
	}

	public static void addVerificationFailure(Throwable e){
		List<Throwable> verificationFailures = getVerificationFailures();
		verificationFailuresMap.put(Reporter.getCurrentTestResult(),verificationFailures);
		if (verificationFailuresMap.containsValue (verificationFailures)){
			e.getMessage ();
		}
		verificationFailures.add(e);
	}



	/**
	 * Using Hard Assert
	 * @param assertType
	 * @param type
	 * @param type1
	 * @param <K>
	 * @param <V>
	 */
	public static<K,V> void hardAssert(AssertType assertType,K type,V type1){
    	try {
			switch (assertType) {
				case ASSERT_EQUALS:
					assertEquals (type, type1);
					System.out.println("Verify Assert Equals Between" + type + "And " + type1);
					break;
				case ASSERT_NOT_EQUALS:
					assertNotEquals (type, type1);
					System.out.println("Verify Assert Not Equals Between" + type + "And " + type1);
					break;
				case ASSERT_SAME:
					assertSame(type, type1);
					System.out.println ("");
					break;
				case ASSERT_NOT_SAME:
					assertNotSame (type, type1);
					System.out.println ("");
					break;
			}
		}catch (Throwable e){
    		ErrorCollector.addVerificationFailure(e);
    		ErrorCollector.getVerificationFailures();
		}
	}

	public static <T> void hardAssert(AssertType assertType,T type){

		switch (assertType) {

			case ASSERT_TRUE:
				assertTrue (type);
				System.out.println ("");
				break;
			case ASSERT_FALSE:
				assertFalse (type);
				System.out.println ("");
				break;
			case ASSERT_NULL:
				assertNull (type);
				System.out.println ("");
				break;
			case ASSERT_NOT_NULL:
				assertNotNull (type);
				System.out.println ("");
				break;
		}
	}



	/**
	 * Using Soft Assert
	 * @param assertType
	 * @param actual
	 * @param expected
	 * @param <V>
	 * @param <K>
	 */
    public static<V,K> void softAssert(AssertType assertType,V actual,K expected){
    	try {
    		switch (assertType){
				case ASSERT_EQUALS:
					softAssert.assertEquals (actual, expected);
					System.out.println("");
				break;
				case ASSERT_NOT_EQUALS:
					softAssert.assertNotEquals (actual,expected);
					System.out.println("");
					break;
				case ASSERT_FALSE:
					softAssert.assertFalse ((Boolean) expected);
					System.out.println("");
					System.out.println("");
					break;
				case ASSERT_TRUE:
					softAssert.assertTrue ((Boolean) expected);
					System.out.println("");
					break;
				case ASSERT_NOT_NULL:
					softAssert.assertNotNull (actual);
					System.out.println("");
					break;
				case ASSERT_NULL:
					softAssert.assertNull(actual);
				case ASSERT_NOT_SAME:
					softAssert.assertNotSame(actual,expected);
					System.out.println("");
					break;
				case ASSERT_SAME:
					softAssert.assertSame (actual,expected);
					System.out.println("");
					break;
			}
			System.out.println ("Verify  : " + actual + " To : " + expected);
		}catch (Throwable e){
    		addVerificationFailure (e);
		}
	}
}
