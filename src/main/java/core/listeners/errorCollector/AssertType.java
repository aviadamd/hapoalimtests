package core.listeners.errorCollector;

public enum AssertType {
    ASSERT_EQUALS,
    ASSERT_NOT_EQUALS,
    ASSERT_FALSE,
    ASSERT_TRUE,
    ASSERT_NOT_SAME,
    ASSERT_SAME,
    ASSERT_NOT_NULL,
    ASSERT_NULL
}
