package core.listeners.errorCollector;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import java.util.List;
import static org.testng.internal.Utils.stackTrace;

/**
 * afterInvocation method:
 * if there are verification failures...
 * set the test to failed
 * if there is an assertion failure add it to verificationFailures
 * if there's only one failure just set that
 * create a failure message with all failures and stack traces (except last failure)
 *
 * beforeInvocation method:
 * only display the following Method that about to run
 * @author Aviad Ben Shemon
 */

public class TestListenerAdapter implements IInvokedMethodListener{



	public void afterInvocation(IInvokedMethod method, ITestResult result){
		Reporter.setCurrentTestResult (result);
		if (method.isTestMethod()){
			List<Throwable> verificationFailures = ErrorCollector.getVerificationFailures();
			if (verificationFailures.size () > 0) {  //if there are verification failures...
				result.setStatus (ITestResult.FAILURE); //set the test to failed
				if (result.getThrowable () != null) { //if there is an assertion failure add it to verificationFailures
					verificationFailures.add (result.getThrowable ());
				}
				int size = verificationFailures.size ();
				if(size == 1){ //if there's only one failure just set that
					result.setThrowable (verificationFailures.get (0));
				}else{//create a failure message with all failures and stack traces (except last failure)
					StringBuffer failureMessage = new StringBuffer("Multiple failures (").append (size).append ("):\n\n");
					for (int i = 0; i < size - 1; i++) {
						failureMessage.append ("Failure ").append (i + 1).append (" of ").append (size).append (":\n");
						Throwable t = verificationFailures.get (i);
						String fullStackTrace = stackTrace (t, false)[1];
						failureMessage.append (fullStackTrace).append ("\n\n");
					}
					Throwable last = verificationFailures.get (size - 1); //final failure
					failureMessage.append ("Failure ").append (size).append (" of ").append (size).append (":\n");
					failureMessage.append (last.toString ());
					Throwable merged = new Throwable (failureMessage.toString ()); //set merged throwable
					merged.setStackTrace (last.getStackTrace ());
					result.setThrowable (merged);
				}
			}
		}
	}

	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1){
		String textMsg = "About to begin executing following method : " + arg0.getTestMethod () + arg1.getStatus ();
		Reporter.log(textMsg,false);
	}

}
