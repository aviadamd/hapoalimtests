package core.listeners.customListner;

import com.aventstack.extentreports.ExtentTest;
import core.base.Base;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;
import java.io.IOException;

/**
 * 
 * @author Aviad Ben Shemon
 *
 */
public class Retry implements IRetryAnalyzer{

	private static ThreadLocal<ExtentTest> test = new ThreadLocal<>();
	public static final Log logger = new Log();
	private int retryCount = 0;
	private int maxRetryCount = 3;

	Retry(){
		super();
	}

	public boolean retry(ITestResult result){
		if (retryCount < maxRetryCount) {
			result.setStatus (ITestResult.FAILURE);
			logger.writeToLogAndConsole (LogLevel.DEBUG,"Retrying test " + result.getName() + " with status "
					+ getResultStatusName(result.getStatus()) + " for the " + (retryCount + 1) + " time(s).");

			Reporter.log("Retrying test " + result.getName() + " with status "
					+ getResultStatusName(result.getStatus()) + " for the " + (retryCount + 1) + " time(s).");
			retryCount++;
			return true;
		}else {
			result.setStatus (ITestResult.SUCCESS);
		}
		return false;
	}

	private String getResultStatusName(int status){
		String resultName = null;
		if(status == 0)
			resultName = "UNKNOWN";
		if(status == 1)
			resultName = "SUCCESS";
		if(status == 2)
			resultName = "FAILURE";
		if(status == 3)
			resultName = "SKIP";
		return resultName;
	}

	private void extentReportsFailOperations(ITestResult iTestResult) throws IOException {
		Object testClass = iTestResult.getInstance();
		WebDriver webDriver = ((Base) testClass).getDriver();
		String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)webDriver).getScreenshotAs(OutputType.BASE64);
		test.get().error(iTestResult.getTestName () + test.get ().addScreenCaptureFromPath(base64Screenshot));
		test.get().addScreenCaptureFromPath (base64Screenshot,"Error Screen Shot");
	}
}
