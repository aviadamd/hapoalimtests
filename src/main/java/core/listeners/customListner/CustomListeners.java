package core.listeners.customListner;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import core.base.Base;
import core.base.BaseUtility;
import helper.actions.MyScreenRecorder;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;
import utilities.extentmenager.ExtentsManager;
import utilities.mailHelper.MailTestConfig;
import utilities.mailHelper.MonitoringMail;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import utilities.mailHelper.SendMailSSLWithAttachment;
import static core.base.BaseUtility.screenshotName;


public class CustomListeners extends Base  implements ITestListener,ISuiteListener{


    private Log logger = new Log();
    private static ExtentTest extentTest;
    private static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
    private String messageBody;



    @Override
    public void onFinish(ITestContext iTestContext){
        System.setProperty("org.uncommons.reportng.escape-output","true");
        Reporter.log("Finnish Tests Method As : " + Arrays.toString(iTestContext.getAllTestMethods()));
        logger.writeToLogAndConsole(LogLevel.INFO,"Finnish Tests Method As : " +
                Arrays.toString(iTestContext.getAllTestMethods()));
        ExtentsManager.getInstance().flush();
    }

    @Override
    public void onStart(ITestContext iTestContext){
        ExtentsManager.getInstance().createTest ("About To Start ","test  -> " + iTestContext.getAllTestMethods ());
        System.setProperty("org.uncommons.reportng.escape-output","true");
        Reporter.log("Start Tests Method As : " + Arrays.toString(iTestContext.getAllTestMethods()));
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult testResult){
        System.setProperty("org.uncommons.reportng.escape-output","true");
        if(testResult.getStatus () == ITestResult.SUCCESS_PERCENTAGE_FAILURE){
            test.get().log(Status.DEBUG,"Test Failed But Within Success Percentage : " + testResult.getTestContext());
            Reporter.log("Test Failed But Within Success Percentage : " + testResult.getTestContext());
        }else if(testResult.getStatus () != ITestResult.SUCCESS_PERCENTAGE_FAILURE){
            Reporter.log("Test Failed But Within Success Percentage Has A Problem: " + testResult.getTestContext());
        }
    }

    @Override
    public void onTestFailure(ITestResult testResult){
        System.setProperty("org.uncommons.reportng.escape-output","true");

        if(!testResult.isSuccess ()){//screen recorder
            try {
                MyScreenRecorder.startRecording (testResult.getTestName());
            } catch (Exception e) {
                e.printStackTrace ();
            }
        }else {
            try {
                MyScreenRecorder.stopRecording ();
            } catch (Exception e) {
                e.printStackTrace ();
            }
        }

        if(!testResult.isSuccess()) {//extent report
            try {
                test.get().log (Status.ERROR, testResult.getTestName () + "Has Failed ");
                test.get().error (testResult.getTestName () + test.get ().addScreenCaptureFromPath (screenshotName));
                test.get().addScreenCaptureFromPath (BaseUtility.screenshotName, "Error Screen Shot");
            } catch (IOException e) {
                logger.writeToLogAndConsole (LogLevel.ERROR, e.getMessage ());
            } catch (NullPointerException n){
                logger.writeToLogAndConsole (LogLevel.ERROR,n.getCause());
            }
        }

        if(!testResult.isSuccess()) {//testng reporter
            try {
                Reporter.log ("Test Failed See Error : " + testResult.getName ());
                BaseUtility.captureScreenshot ();
                Reporter.log ("Click to see Screenshots");
                Reporter.log ("<a target=\"_blank\" href=" + BaseUtility.screenshotName + " >Screenshots</a>");
                Reporter.log ("<br>");
                Reporter.log ("<br>");
                Reporter.log ("<a target=\"_blank\" href=" + BaseUtility.screenshotName + "><img src=" + BaseUtility.screenshotName + " height=200 width=200></img></a>");
            } catch (IOException e) {
                e.printStackTrace ();
            }
        }
    }


    @Override
    public void onTestSkipped(ITestResult testResult){
        System.setProperty("org.uncommons.reportng.escape-output","true");
        if(testResult.getStatus () == ITestResult.SKIP){
            Reporter.log ("Test Skipped As : " + testResult.getTestName ());
        }
    }

    @Override
    public void onTestStart(ITestResult result) {
        if(result.getStatus () == ITestResult.STARTED){
            ExtentTest extentTest = ExtentsManager.getInstance().createTest(result.getMethod().getMethodName(),result.getMethod().getDescription());
            test.set(extentTest);
            test.get().info ("Test Is Started ->  " + result.getStartMillis());
        }
        System.setProperty("org.uncommons.reportng.escape-output","true");
        Reporter.log ("Test : " + result.getName () + "As Started");

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.setProperty("org.uncommons.reportng.escape-output","true");
        if(result.getStatus() == ITestResult.SUCCESS){
            test.get().pass(result.getTestName());
            test.get().pass("Test Pass" + result.getStatus());
         }
        Reporter.log ("Tests Success Are : " + result.getTestName());
    }

    @Override
    public void onFinish(ISuite suite) {
        System.setProperty("org.uncommons.reportng.escape-output","true");
        SendMailSSLWithAttachment.SendMail("Sending Mail Report After Finnish Tests Suites" + suite.getAllMethods ());
        MonitoringMail mail = new MonitoringMail();
        try {
            messageBody = "C:\\FrameWorksAndMasters\\com.hapoalim.tests\\target\\surefire-reports\\html\\extent_report.html";
            mail.sendMail (MailTestConfig.server, MailTestConfig.from,
                    MailTestConfig.to, MailTestConfig.subject, messageBody);
            Reporter.log ("Finish Tests A Mail As Sent With Reports" + suite.getAllInvokedMethods ());
        } catch (Exception w){
            w.getCause ();
        }
    }

    @Override
    public synchronized void onStart(ISuite suite) {
        System.setProperty("org.uncommons.reportng.escape-output","true");
        Reporter.log ("Start Tests" + suite.getAllMethods());
    }
}
