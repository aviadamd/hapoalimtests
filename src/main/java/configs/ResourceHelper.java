package configs;

import core.base.manegers.ServerManager;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;

public class ResourceHelper {

    private static String getBaseResourcePath(){
        String path = System.getProperty("user.dir");
        System.out.println(path);
        return path;
    }


    private static String getResourcePath(String resource){
        String path = getBaseResourcePath() + resource;
        return path;
    }


    /**
     * call only this method
     * @param path
     * @return
     * @throws FileNotFoundException
     */
    public static InputStream getResourcePathInputStream(String path) throws FileNotFoundException {
        return new FileInputStream(ResourceHelper.getResourcePath(path));
    }


    /**
     * down below refer to server manager class -->
     */
    final public static String QUEUE = ServerManager.getWorkingDir()+"/src/test/resources/logs/queue.json";

    public static JSONObject getQueue() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader (QUEUE));
        return (JSONObject) obj;
    }
}
