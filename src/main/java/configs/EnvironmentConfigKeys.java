
package configs;

/**
 * @author aviad
 * @since Dec 11, 2018
 */
public interface EnvironmentConfigKeys {
	/**
	 * Config key.
	 */
	String JSON_SYSTEM = "C:\\FrameWorksAndMasters\\NewProject\\com.hapoalim.tests\\src\\test\\resources\\data\\system.json";
	String JSON_CAPABILITIES = "C:\\FrameWorksAndMasters\\NewProject\\com.hapoalim.tests\\src\\test\\resources\\data\\capabilities.json";
    String JSON_CONSTANTS = "C:\\FrameWorksAndMasters\\NewProject\\com.hapoalim.tests\\src\\test\\resources\\data\\constants.json";
	String NodeModule = System.getProperty ("user.dir") + "\\node module";
	String NodeJs = System.getProperty ("user.dir") + "\\node js";
	String ScreenShotPath = "C:\\FrameWorksAndMasters\\NewProject\\com.hapoalim.tests\\src\\main\\resources\\screenshots";
    String ScreenShot = "todo";
}