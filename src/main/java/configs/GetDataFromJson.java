package configs;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.junit.JUnitTestClass;
import utilities.customlogger.Log;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;


public class GetDataFromJson {


    private Log logger = new Log ();



    /**
     * Capabilities Json
     */
    public static String Udid = "";
    public static String DeviceName = "";
    public static String AppActivity = "";
    public static String AppPackage = "";
    public static String ResetChose = "";
    public static String UnicodeKeyBoard = "";
    public static String AutomationName = "";
    public static String ResetKeyBoard = "";
    public static String Url = "";
    public static String PageLoadTimeout = "";


    /**
     * Constants Json
     */
    public static String Project = "AccountManagement";
    public static String DeviceType = "";
    public static String PlatformType = "";
    public static String TestingEnvironment = "";
    public static String ServerNameAccountManagement = "";
    public static String ServerNumberAccountManagement = "";
    public static String UserNameForProdAccountManagement = "";
    public static String PasswordForProdAccountManagement = "";



    public static void readFromPropertiesJson (){
        JSONParser parser = new JSONParser ();
        try {
            Reader reader = new FileReader (EnvironmentConfigKeys.JSON_CAPABILITIES);
            JSONObject jsonObj = (JSONObject) parser.parse (reader);
            Udid = jsonObj.get ("udid").toString();
            DeviceName = jsonObj.get ("deviceName").toString ();
            AppActivity = jsonObj.get ("appActivity").toString ();
            AppPackage = jsonObj.get ("app_package").toString ();
            ResetChose = jsonObj.get ("resetChose").toString ();
            UnicodeKeyBoard = jsonObj.get ("unicodeKeyBoard").toString ();
            AutomationName = jsonObj.get ("automationName").toString ();
            Url = jsonObj.get ("url").toString ();
            //////////////////////////////////////////////////////////////////////////////////
            Reader reader2 = new FileReader (EnvironmentConfigKeys.JSON_CONSTANTS);
            JSONObject jsonObj2 = (JSONObject) parser.parse (reader2);
            Project = jsonObj2.get("project").toString();
            PlatformType = jsonObj2.get("platformType").toString();
            DeviceType = jsonObj2.get("deviceType").toString();
            TestingEnvironment = jsonObj2.get("testingEnvironment").toString();
            ServerNameAccountManagement = jsonObj2.get("serverNameAccountManagement").toString();
            ServerNumberAccountManagement = jsonObj2.get("serverNumberAccountManagement").toString();
            UserNameForProdAccountManagement = jsonObj2.get("userNameForProdAccountManagement").toString();
            PasswordForProdAccountManagement = jsonObj2.get("passWordForProdAccountManagement").toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace ();
        } catch (IOException io) {
            io.getCause ();
        } catch (ParseException e) {
            e.printStackTrace ();
        }
    }
}
