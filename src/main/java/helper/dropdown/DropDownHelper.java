
package helper.dropdown;

import core.base.Base;
import io.appium.java_client.android.AndroidElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Aviad Ben Shemon
 */

public class DropDownHelper extends Base {

    private Logger Log = Logger.getLogger(DropDownHelper.class);

	public void SelectUsingVisibleValue(WebElement element, String visibleValue){
		Select select = new Select (element);
		select.selectByVisibleText(visibleValue);
		Log.info("Locator : " + element + " Value : " + visibleValue);
	}

	public String getSelectedValue(WebElement element) {
		String value = new Select (element).getFirstSelectedOption().getText();
		Log.info("WebELement : " + element + " Value : "+ value);
		return value;
	}
	
	public void SelectUsingIndex(WebElement element, int index) {
		Select select = new Select (element);
		select.selectByIndex(index);
		Log.info("Locator : " + element + " Value : " + index);
	}
	
	public void SelectUsingVisibleText(WebElement element, String text) {
		Select select = new Select (element);
		select.selectByVisibleText(text);
		Log.info("Locator : " + element + " Value : " + text);
	}
	
	
	public List<String> getAllDropDownValues(WebElement locator) {
		Select select = new Select (locator);
		List<WebElement> elementList = select.getOptions();
		List<String> valueList = new LinkedList<String>();
		
		for (WebElement element : elementList) {
			Log.info(element.getText());
			valueList.add(element.getText());
		}
		return valueList;
	}


	public void select3DatePicker(WebElement element1, String keysToSend, String keysToSend1, String keysToSend2) {
		try {
			Log.info ("Attmpet to Start 3Date Picker Test");
			List <AndroidElement> element = driver.findElements (By.id (String.valueOf (element1)));
			element.get (0).sendKeys (keysToSend);
			element.get (1).sendKeys (keysToSend1);
			element.get (2).sendKeys (keysToSend2);
			Log.info ("Type Into Filed With : " + element1 + "And : " + keysToSend + "And : " + keysToSend1 + "And : " + keysToSend2);
		} catch (Exception e) {
			Log.info ("Cant Type To Date Picker See Exception : " + e.getCause ());
		}
	}

	public void selectDropListByValue (WebElement element, String value) {
		try {
			Select dropList = new Select (element);
			dropList.selectByValue (value);
			Log.info ("Attempt To Take Element From Drop List : " + element + "With The Value : " + value);
		} catch (Exception e) {
			Log.info ("element not selected" + e.getMessage ());
		}
	}

	public void selectDropListByIndex (WebElement element,int index) {
		try {
			Select dropList = new Select(element);
			Log.info("Selected By Element : " + element);
			dropList.selectByIndex(index);
			Log.info("Selected By Index : " + index);
		} catch (Exception e) {
			Log.info ("element not selected : " + e.getCause ());
		}
	}

	public void selectDropListByVisibleText (WebElement element, String visibleText) {
		try {
			Select dropList = new Select(element);
			dropList.selectByVisibleText(visibleText);
		} catch (Exception e) {
			Log.info ("element not selected : " + e.getCause ());
		}
	}

	public void printAllElementOnDropList (WebElement element,WebElement s) {
		try {
			Select printDropList = new Select (element);
			List <WebElement> MyDropList = printDropList.getOptions ();
			int listSize = MyDropList.size ();
			for (WebElement aMyDropList : MyDropList) {
				String valueOfCombo = aMyDropList.getText ();
				if(valueOfCombo.contains ((CharSequence) s))
					element.click ();
				System.out.println (listSize);
			}
		} catch (Exception e) {
			Log.info ("cant Print Elements : " + e.getCause ());
		}
	}
}
