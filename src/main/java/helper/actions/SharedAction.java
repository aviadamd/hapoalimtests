package helper.actions;

import configs.EnvironmentConfigKeys;
import core.base.Base;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;


public class SharedAction {

    public static AppiumDriver appiumDriver = (AppiumDriver) Base.driver;
    public static Dimension size = Base.driver.manage().window().getSize();
    public WebDriver driver;

    // Wait for element functions

    public static boolean elementToBeClickable(WebElement webElement) {

        WebDriverWait wait = new WebDriverWait(appiumDriver, 20);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(webElement));
            System.out.println(webElement + " is available for click");
            return true;
        } catch (Exception e) {
            System.out.println(webElement + " is not available for click");
            return false;
        }
    }


    public static void waitForElement(WebElement element, Integer timeout) {
        FluentWait elementWaiter = new FluentWait(Base.driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        elementWaiter.until(ExpectedConditions.visibilityOf(element));
    }



    public static void waitFewSeconds(Integer seconds) {
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static boolean isElementPresented(WebElement w, int timeOut) {
        boolean isElementPresented = false;
        try {
            waitForElement(w, timeOut);
            if (w.isDisplayed()) {
                isElementPresented = true;
            }
        } catch (Exception e) {
            System.out.println(" No Such Element found here - details below:");
            System.out.println("" + e.getMessage().toString());
            isElementPresented = false;
            return isElementPresented;
        }
        return isElementPresented;
    }


//    // Touch Actions - Scroll, Swipe and more
//
//    public static void swipeGestureFromLeftToRight(int timesToPerformSwipe) {
//        for (int i = 0; i < timesToPerformSwipe; i++) {
//            try {
//                TouchAction touchAction = new TouchAction((AppiumDriver) Base.driver);
//                TouchAction perform = touchAction.press (100, size.getHeight () / 2).waitAction (Duration.ofMillis (1000)).moveTo (1000, size.getHeight () / 2).release ().perform ();
//
//            } catch (Exception e) {
//                System.out.println("OMG - Couldn't swipe any more");
//                i = timesToPerformSwipe;
//                System.out.println("Done Swiping for " + i + " times");
//            }
//        }
//    }

    public static void longPressElement(WebElement webElement) {
        TouchAction touchAction = new TouchAction((AppiumDriver) Base.driver);
        touchAction.longPress((PointOption) webElement).release().perform();
    }
    // Driver Actions

    public static void closeKeyBoard() {
        appiumDriver.hideKeyboard();
    }

    public static List<AndroidElement> getWebElementsList(By by) {
        return Base.driver.findElements (by);
    }

    public static void backButtonAndroid() {
        appiumDriver.navigate().back();
    }

    public static void closeApp() {
        appiumDriver.closeApp();
    }

    public static void launchApp() {
        appiumDriver.launchApp();
    }

    public static void clearCache() {
        appiumDriver.resetApp();
    }

    // Just for android
    public static void enterAndroidButton() {
        AndroidDriver androidDriver = (AndroidDriver) Base.driver;
        androidDriver.pressKeyCode(AndroidKeyCode.ENTER);
    }

    // Just for android
    public static void homeAndroidButton() {
        AndroidDriver androidDriver = (AndroidDriver) Base.driver;
        androidDriver.pressKeyCode(AndroidKeyCode.HOME);
    }

    // General Functions

    public static void screenshot() throws IOException {
        String scrFolder = EnvironmentConfigKeys.ScreenShotPath;
        File scrFile = ((TakesScreenshot) Base.driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(scrFolder + Calendar.getInstance().getTime() + ".jpg"));
    }

    public static void setTextInTextField(String text, WebElement element) {
        element.clear();
        CharSequence seq = text;
        element.sendKeys(seq);
    }

    public static void clearTextInTextField(WebElement element) {
        element.clear();
    }

    // Just for android
    public static void scrollByText(String byText) {
        try {
            AndroidDriver androidDriver = (AndroidDriver) Base.driver;
            androidDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"" + byText + "\"));");
        } catch (Exception e) {
            waitFewSeconds(3000);
        }
    }

    public static void addFingerPrint(String fingerNumber) {
        try {
            Runtime.getRuntime().exec("adb devices to get emulator id -->emulator-5554");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Runtime.getRuntime().exec("adb -s emulator-5554 emu finger touch " + fingerNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void fingerPrintAuthentication(String fingerNumber) {

        try {
            SharedAction.waitFewSeconds(2000);
            Runtime.getRuntime().exec("adb -e emu finger touch " + fingerNumber);
        } catch (IOException e) {
            SharedAction.waitFewSeconds(5000);
        }
    }

    public static void clickOnSpecificItemByClassName(String byClass, int option) {

        Base.driver.findElements(By.className(byClass)).get(option).click();

    }

    public static void clickOnSpecificItemByXpath(String byXpath, int option) {

        Base.driver.findElements(By.xpath(byXpath)).get(option).click();

    }


    // Just for android
    public static void getCurrentActivity(){

        AndroidDriver androidDriver = (AndroidDriver) Base.driver;
        System.out.println(androidDriver.currentActivity());

    }

    // Just for android
    public static String getIdOfWebElementForAndroid(WebElement webElementForAndroid) {
        String retVal = "";
        try {
            retVal = webElementForAndroid.toString().split("hapoalim android id")[1];
            retVal = retVal.split(" \n")[0];
            retVal = retVal.split("\"")[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retVal;
    }

    public static void CloseKeyBoard() {
        appiumDriver.hideKeyboard();
    }
}




