package helper.actions;

public enum TypeCommands{
    CLICK,
    SEND_KEYS,
    SUBMIT,
    CLEAR_TEXT
}
