package helper.actions;


import helper.assertionHelper.VerifyControllers;
import io.appium.java_client.AppiumDriver;
import org.omg.CORBA.portable.ApplicationException;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;
import org.testng.annotations.Test;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class Commands {

    private static Log logger = new Log ();
    private static WebDriver driver;
    private static AppiumDriver appiumDriver;


    public static WebElement click (WebElement element) throws Exception {
        try {
            if (VerifyControllers.isElementPresented (element, 20)) {
                System.setProperty ("org.uncommons.reportng.escape-output", "true");
                Reporter.log ("About To Click On : " + element.getText ());
                logger.writeToLogAndConsole (LogLevel.DEBUG, "Clicking on an Element : " + element);
                element.click ();
            } else if (!VerifyControllers.isElementPresented (element, 20)) {
                logger.writeToLogAndConsole (LogLevel.DEBUG, "Clicking on an Element : " + element);
            } else throw new Exception ();
        } catch (NoSuchElementException | StackOverflowError e) {
            logger.writeToLogAndConsole (LogLevel.ERROR, e.getMessage ());
        } catch (TimeoutException | ApplicationException e) {
            logger.writeToLogAndConsole (LogLevel.ERROR, e.getStackTrace ());
        }
        return element;
    }


    public static WebElement submit (WebElement element) {

        System.setProperty ("org.uncommons.reportng.escape-output", "true");
        logger.writeToLogAndConsole (LogLevel.DEBUG, "Submit in an Element : " + element);
        Reporter.log ("About To Submit On : " + element.getText ());
        element.submit ();
        return element;
    }


    public static WebElement typeKeysIntoFiled (WebElement element, String value, WebElement element1, String value1) {
        System.setProperty ("org.uncommons.reportng.escape-output", "true");
        logger.writeToLogAndConsole (LogLevel.DEBUG, "Typing in an Element : " + element + " entered value as : " + value);
        logger.writeToLogAndConsole (LogLevel.DEBUG, "Typing in an Element : " + element1 + " entered value as : " + value1);
        Reporter.log ("Typing in an Element : " + element1 + " entered value as : " + value1);
        element.sendKeys (value);
        element.sendKeys (value1);
        return element;
    }


    public static WebElement typeKeysIntoFiled (WebElement element, String value) {
        System.setProperty ("org.uncommons.reportng.escape-output", "true");
        logger.writeToLogAndConsole (LogLevel.DEBUG, "Typing in an Element : " + element + " entered value as : " + value);
        Reporter.log ("Typing in an Element : " + element + " entered value as : " + value);
        element.sendKeys (value);
        return element;
    }

    public static void clearText (WebElement element) {
        element.clear ();
    }

    /*
    verify is text filed is empty if it is it will clear the text
     */
    public static void clearTextIfTextFiledIsFull (WebElement element, String elementId, String elementAtrribute, String atrribute) {
        try {
            {
                WebElement textFiled = driver.findElement (By.id (elementId));
                if (textFiled.getAttribute (elementAtrribute).equals (atrribute)) {
                    element.clear ();
                    logger.writeToLogAndConsole (LogLevel.DEBUG, "Clear Text Action");
                } else {
                    logger.writeToLogAndConsole (LogLevel.ERROR, "");
                }

            }
        } catch (Exception e) {
            logger.writeToLogAndConsole (LogLevel.DEBUG, "cant clear text error is print : " + e.getMessage ());
        }
    }


    public static void clickThisOnElement (WebElement element) {
        try {
            System.setProperty ("org.uncommons.reportng.escape-output", "true");
            if (element.isDisplayed () || element.isEnabled ()) {
                Reporter.log (" Clicking on an Element As : " + element);
                logger.writeToLogAndConsole (LogLevel.DEBUG, " Clicking on an Element As : " + element);
                element.click ();
            } else {
                logger.writeToLogAndConsole (LogLevel.DEBUG_ERROR, "Cant Click On This Element ");
            }
        } catch (NoSuchElementException e) {
            logger.writeToLogAndConsole (LogLevel.ERROR_ERROR, "See Exception Message : " + e.getMessage ());
            Reporter.log ("See Exception Message : " + e.getMessage ());
        }
    }


    public void submitClickFunction (WebElement element) {
        try {
            if (element.isDisplayed () || element.isEnabled ()) {
                logger.writeToLogAndConsole (LogLevel.INFO, " Clicking on an Element : " + element);
                element.submit ();
            }
        } catch (NoSuchElementException e) {
            fail ("Fail See Exception" + e.getMessage ());
            logger.writeToLogAndConsole (LogLevel.DEBUG_ERROR, "See Exception Message : " + e);
        } catch (Throwable throwable) {
            logger.writeToLogAndConsole (LogLevel.ERROR_ERROR, "Cannot Submit See Error Message : " + throwable);
        }
    }


    public static void typeToThisTextField (WebElement element, String UserName) {
        System.setProperty ("org.uncommons.reportng.escape-output", "true");
        try {
            if (element.isDisplayed () || element.isEnabled ()) {
                element.sendKeys (UserName);
                Reporter.log ("Typing in an Element " + element + "With The Credentials" + UserName);
                logger.writeToLogAndConsole (LogLevel.DEBUG_ERROR, "Typing in an Element " + element + "With The Credentials" + UserName);
            }
        } catch (NoSuchElementException e) {
            logger.writeToLogAndConsole (LogLevel.ERROR_ERROR, "Cant Type Into Text Filed : " + e.getCause ());
        }
    }


    public static void resetApp () {
        appiumDriver.resetApp ();
    }


    public static WebDriver closeApp () {
        System.setProperty ("org.uncommons.reportng.escape-output", "true");
        try {
            if (driver != null) {
                logger.writeToLogAndConsole (LogLevel.INFO, "try to close app after the test");
                driver.close ();
            } else {
                logger.writeToLogAndConsole (LogLevel.DEBUG_DEBUG, "cant close app see exception");
                throw new Exception ();
            }
        } catch (Exception e) {
            System.out.println ("exception : " + e.getCause ());
        }
        return null;
    }


    public static void hideKeyBoard () {
        appiumDriver.hideKeyboard ();
    }


    public static void clickWhileElementPresent (WebElement element, WebElement object) {
        do {
            element.click ();
        } while (VerifyControllers.isElementPresented (object, 10));
    }


    public static void sendKeysWhileElementPresent (WebElement element, String object, WebElement element1) {
        do {
            element.sendKeys (object);
        } while (VerifyControllers.isElementPresented (element1, 10));
    }


    private <T> void verifyThatElementNotPresented (T type) {
        List<WebElement> element = driver.findElements (By.id (String.valueOf (type)));
        assertTrue (element.size () == 0);
    }



}
