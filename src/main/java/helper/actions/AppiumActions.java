package helper.actions;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.GsmCallActions;
import org.apache.log4j.Logger;
import org.openqa.selenium.DeviceRotation;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;

import static javax.imageio.ImageIO.*;

public class AppiumActions {

    private AppiumDriver driver;
    private AndroidDriver androidDriver;

    private void setAndroidDriver (AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    public void iosSiriCommands (String Command) {
        HashMap<String, String> args = new HashMap<> ();
        args.put ("text", Command);
        driver.executeScript ("mobile: siriCommand", args);
    }

    public void makeGsmCall (String enterPhone) {
        System.out.println("enter phone As : " + enterPhone);
        androidDriver.makeGsmCall (enterPhone, GsmCallActions.CALL);
    }

    private String getReferenceImageB64(String PicSource)throws URISyntaxException, IOException {
        URL refImgUrl = getClass().getClassLoader().getResource(PicSource);
        File refImgFile = Paths.get(refImgUrl.toURI ()).toFile();
        return Base64.getEncoder ().encodeToString (Files.readAllBytes (refImgFile.toPath ()));
    }

    public BufferedImage takeShot () {
        for (int i = 0; i < 5; i++) {
            if (driver.manage ().window ().getSize ().getHeight () > driver.manage ().window ().getSize ().getWidth ()) {
                System.out.println(" takeShot: portrait mode w: " + driver.manage ().window ().getSize ().getWidth ()
                        + ", h: " + driver.manage ().window ().getSize ().getHeight ());
                try {
                    return read(driver.getScreenshotAs (OutputType.FILE));
                } catch (Exception e) {
                    e.getCause ().getMessage ();
                }
            } else {
                System.out.println(" takeShot: landscape mode w: " + driver.manage ().window ().getSize ().getWidth ()
                        + ", h: " + driver.manage ().window ().getSize ().getHeight ());
                BufferedImage image = null;
                try {
                    image = read (driver.getScreenshotAs (OutputType.FILE));
                } catch (Exception e) {
                    System.err.println ("    get screenshot failed.");
                }
                if (image != null) {
                    System.out.println("original image height: " + image.getHeight () + ", width: " + image.getWidth ());
                    int diff = (image.getHeight () - image.getWidth ());
                    diff = diff / 2;
                    System.out.println("   diff: "+diff);
                    AffineTransform tx = new AffineTransform ();
                    tx.rotate (Math.PI * 1.5, image.getHeight () / 2, image.getWidth () / 2);//(radian,arbit_X,arbit_Y)
                    AffineTransformOp op = new AffineTransformOp (tx, AffineTransformOp.TYPE_BILINEAR);
                    image = op.filter (image, null);//(source,destination)
                    System.out.println("    rotated image height: "+image.getHeight()+", width: "+image.getWidth());

                    image = image.getSubimage (diff, diff, image.getWidth () - diff, image.getHeight () - diff);
                    System.out.println("  result image height: "+image.getHeight()+", width: "+image.getWidth());
                    return image;
                }
            }
        }
        return null;
    }


    public void makeGsmAccept (String enterPhone) {
        DesiredCapabilities caps = new DesiredCapabilities ();
        androidDriver.makeGsmCall (enterPhone, GsmCallActions.ACCEPT);
    }

    public void makeGsmCancel (String enterPhone) {
        androidDriver.makeGsmCall (enterPhone, GsmCallActions.CANCEL);
    }

    public void makeGsmHold (String enterPhone) {
        androidDriver.makeGsmCall (enterPhone, GsmCallActions.HOLD);
    }

    private  <T>boolean isAppInstalledTest (T APP) {
        AssertJUnit.assertTrue (driver.isAppInstalled ((String) APP));
        return driver.isAppInstalled ((String) APP);
    }

    private boolean isAppNotInstalledTest (String APP) {
        AssertJUnit.assertFalse (driver.isAppInstalled (APP));
        return driver.isAppInstalled (APP);
    }

    public void installAppFunc(String APP, String APP_PATH) {
        if (isAppInstalledTest (APP) || isAppNotInstalledTest (APP)) {
            driver.installApp (APP_PATH);
        }
    }

    public void hideKeyBoard () {

        driver.hideKeyboard ();
    }

    public void rotateDevice (int x, int y, int z) {

        driver.rotate (new DeviceRotation (x, y, z));
    }

    public void getDevicesTime () {

        driver.getDeviceTime ();
    }

}

