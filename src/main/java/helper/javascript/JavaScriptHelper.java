
package helper.javascript;

import core.base.Base;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;


import java.util.HashMap;

/**
 * @author Aviad Ben Shemon
 */

public class JavaScriptHelper extends Base
{

	private WebDriver driver;
	private Logger Log = Logger.getLogger(JavaScriptHelper.class);

	public JavaScriptHelper(WebDriver driver) {

		this.driver = driver;
		Log.debug("JavaScriptHelper : " + this.driver.hashCode());
	}

	public Object executeScript(String script) {

		JavascriptExecutor exe = (JavascriptExecutor) driver;
		Log.info(script);
		return exe.executeScript(script);
	}

	public Object executeScript(String script, Object... args) {

		JavascriptExecutor exe = (JavascriptExecutor) driver;
		Log.info(script);
		return exe.executeScript(script, args);
	}

	public void scrollToElement(WebElement element) {

		executeScript("window.scrollTo(arguments[0],arguments[1])", element.getLocation().x, element.getLocation().y);
		Log.info(element);
	}

	public void scrollToElemetAndClick(WebElement element) {

		scrollToElement(element);
		element.click();
		Log.info(element);
	}

	public void scrollIntoView(WebElement element) {
		executeScript("arguments[0].scrollIntoView()", element);
		Log.info(element);
	}

	public void scrollIntoViewAndClick(WebElement element) {
		scrollIntoView(element);
		element.click();
		Log.info(element);
	}

	public void scrollDownVertically() {

		executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void scrollUpVertically() {

		executeScript("window.scrollTo(0, -document.body.scrollHeight)");
	}

	public void scrollDownByPixel() {

		executeScript("window.scrollBy(0,1500)");
	}

	public void scrollUpByPixel() {

		executeScript("window.scrollBy(0,-1500)");
	}

	public void ZoomInBypercentage() {

		executeScript("document.body.style.zoom='40%'");
	}

	public void ZoomBy100percentage() {

		executeScript("document.body.style.zoom='100%'");
	}

	public void performTapAction(WebElement elementToTap) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, Double> tapObject = new HashMap <> ();
		tapObject.put("x", (double) 360); // in pixels from left
		tapObject.put("y", (double) 170); // in pixels from top
		tapObject.put("element", Double.valueOf(((RemoteWebElement) elementToTap).getId()));
		executeScript("mobile: tap", tapObject);
	}

	public void scrollPageUp(Double StartX,Double StartY,Double EndX,Double EndY,Double Duration) {
		HashMap<String, Double> swipeObject = new HashMap <String, Double>();
		swipeObject.put("startX", StartX);
		swipeObject.put("startY", StartY);
		swipeObject.put("endX", EndX);
		swipeObject.put("endY", EndY);
		swipeObject.put("duration", Duration);
		executeScript("mobile: swipe", swipeObject);
	}

	public void scrollPageDown(Double StartX,Double StartY,Double EndX,Double EndY,Double Duration) {
		HashMap<String, Double> swipeObject = new HashMap <String, Double>();
		swipeObject.put("startX", StartX);
		swipeObject.put("startY", StartY);
		swipeObject.put("endX", EndX);
		swipeObject.put("endY", EndY);
		swipeObject.put("duration", Duration);
		executeScript("mobile: swipe", swipeObject);
	}

	public void swipeLeftToRight() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap <String, Double> swipeObject = new HashMap <String, Double>();
		swipeObject.put("startX", 0.01);
		swipeObject.put("startY", 0.5);
		swipeObject.put("endX", 0.9);
		swipeObject.put("endY", 0.6);
		swipeObject.put("duration", 3.0);
		js.executeScript("mobile: swipe", swipeObject);
	}

	public void swipeRightToLeft() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap <String, Double> swipeObject = new HashMap <String, Double>();
		swipeObject.put("startX", 0.9);
		swipeObject.put("startY", 0.5);
		swipeObject.put("endX", 0.01);
		swipeObject.put("endY", 0.5);
		swipeObject.put("duration", 3.0);
		js.executeScript("mobile: swipe", swipeObject);
	}

	public void swipeFirstCarouselFromRightToLeft() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap <String, Double> swipeObject = new HashMap <String, Double>();
		swipeObject.put("startX", 0.9);
		swipeObject.put("startY", 0.2);
		swipeObject.put("endX", 0.01);
		swipeObject.put("endY", 0.2);
		swipeObject.put("duration", 3.0);
		js.executeScript("mobile: swipe", swipeObject);
	}

	//Code to Swipe DOWN
	public boolean swipeFromUpToBottom(WebElement ObjectToScroll) {
		try {
			HashMap <String, String> scrollObject = new HashMap <String, String>();
			scrollObject.put("direction", "up");
			executeScript("scroll", ObjectToScroll);
			System.out.println("Swipe up was Successfully done.");
		} catch (Exception e) {

			System.out.println("swipe up was not successful");
		}

		return false;
	}

	//Code to Swipe DOWN
	public boolean swipeFromBottonToUp(WebElement ObjectToScroll) {
		try {
			HashMap <String, String> scrollObject = new HashMap <String, String>();
			scrollObject.put("direction", "down");
			executeScript("scroll", ObjectToScroll);
			System.out.println("Swipe up was Successfully done.");
		} catch (Exception e) {

			System.out.println("swipe up was not successful");
		}

		return false;
	}
}
