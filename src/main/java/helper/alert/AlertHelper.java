package helper.alert;


import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;

/**
 * @author Aviad Ben Shemon
 */
public class AlertHelper{



	private WebDriver driver;
	private Log logger = new Log();



	public AlertHelper(WebDriver driver) {
		this.driver = driver;
		logger.writeToLogAndConsole (LogLevel.INFO,"AlertHelper : " + this.driver.hashCode());
	}


	private Alert getAlert() {
		return driver.switchTo().alert();
	}

	private void AcceptAlert() { getAlert().accept();}


	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}


	public void DismissAlertIfPresent() {
		if (!isAlertPresent())
			return;
		DismissAlert();
	}

	public void AcceptPromptAndSendTextToAlert(String text) {

		if (!isAlertPresent()) {
			logger.writeToLogAndConsole (LogLevel.DEBUG,"");
			return;
		}else{
			Alert alert = getAlert ();
			alert.sendKeys (text);
			alert.accept ();
			logger.writeToLogAndConsole(LogLevel.DEBUG,text);
		}
	}

	private void DismissAlert() {
		getAlert().dismiss();
	}


	public String getAlertText() {
		String text = getAlert().getText();
		logger.writeToLogAndConsole(LogLevel.DEBUG,text);
		return text;
	}


	public void AcceptAlertIfPresent() {
		if (!isAlertPresent())
			return;
		    AcceptAlert();
	}

}
