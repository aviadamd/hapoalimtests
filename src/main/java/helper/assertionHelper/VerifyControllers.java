package helper.assertionHelper;

import core.base.Base;
import core.base.BaseUtility;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class VerifyControllers{


    private static SoftAssert softAssert = null;
    private static Log logger = new Log();


    public static boolean verifyElementPresent (WebElement element){
        boolean isDisplayed;
        try {
            element.isDisplayed();
            logger.writeToLogAndConsole(LogLevel.INFO, element.getText () + " is displayed");
            isDisplayed = true;
        } catch (Exception ex) {
            logger.writeToLogAndConsole(LogLevel.ERROR, "Element not found " + ex.getMessage ());
            isDisplayed = false;
        }
        try {
            if (isDisplayed) {
                logger.writeToLogAndConsole(LogLevel.DEBUG, "Element is Presented");
                return true;
            } else {
                logger.writeToLogAndConsole(LogLevel.ERROR, "Element is Not Presented");
                return false;
            }
        } catch (NoSuchElementException e) {e.getMessage ();}
        return isDisplayed;
    }


    public static boolean verifyElementNotPresent (WebElement element){
        boolean isDisplayed;
        try {
            element.isDisplayed();
            logger.writeToLogAndConsole(LogLevel.INFO, element.getText () + " is displayed");
            isDisplayed = false;
        } catch (Exception ex) {
            logger.writeToLogAndConsole(LogLevel.ERROR, "Element not found " + ex.getMessage ());
            isDisplayed = true;
        }
        try {
            if (isDisplayed) {
                logger.writeToLogAndConsole(LogLevel.DEBUG, "Element is Presented");
                return false;
            } else {
                logger.writeToLogAndConsole(LogLevel.ERROR, "Element is Not Presented");
                return true;
            }
        } catch (NoSuchElementException e) {e.getMessage ();}
        return isDisplayed;
    }

    public static boolean verifyElementPresentions(boolean ifTrueThanPresentIfFalseNotPresent,WebElement e){
        if(ifTrueThanPresentIfFalseNotPresent){
            try{
                e.isDisplayed();
                Assert.assertNotNull(e);
                Assert.assertTrue(e.isDisplayed());
                return true;
            }catch(NoSuchElementException noSuchElementException){
                logger.writeToLogAndConsole (LogLevel.ERROR,noSuchElementException.getMessage());
            }
        }else {
            try {
                Assert.assertFalse(e.isDisplayed());
                Assert.assertNull(e);
                return true;
            } catch (AssertionError assertionError) {
                logger.writeToLogAndConsole (LogLevel.ERROR, assertionError.getMessage ());
            }
        }
        return true;
    }



    public static boolean verifyTextEquals(WebElement element, String expectedText) {
        try {
            String actualText = element.getText ();
            if (actualText.equals (expectedText)) {
                logger.writeToLogAndConsole (LogLevel.DEBUG, "actualText is :" + actualText + " expected text is: " + expectedText);
                return true;
            } else {
                logger.writeToLogAndConsole (LogLevel.DEBUG_ERROR, "actualText is :" + actualText + " expected text is: " + expectedText);
                return false;
            }
        } catch (Exception ex) {
            logger.writeToLogAndConsole (LogLevel.DEBUG, "actualText is : "
                    + element.getText () + " expected text is: " + expectedText);
            logger.writeToLogAndConsole (LogLevel.ERROR, "text not matching" + ex.getMessage ());
        }
        return true;
    }


    public static void verifyTexts (WebElement element) {
        logger.writeToLogAndConsole (LogLevel.DEBUG, "Verify if Any Notification is PopUp");
        try {
            if (element.isDisplayed () || element.isEnabled ()) {
                logger.writeToLogAndConsole (LogLevel.DEBUG,
                        "Text Is Displayed See Notification Details : " + element);
                element.getTagName ();
            } else {
                logger.writeToLogAndConsole (LogLevel.ERROR, "Text Is Not Displayed Moving On : ");
            }
        } catch (Exception e) {
            logger.writeToLogAndConsole (LogLevel.ERROR, "See Error " + e.getLocalizedMessage ());
        }
    }

    public static void verifyElementEqualsTo(WebElement element, String Text) {
        try {
            logger.writeToLogAndConsole (LogLevel.INFO, "Check If Element Is Displayed");
            if (element.getText().equals (Text))
            logger.writeToLogAndConsole (LogLevel.DEBUG, "Element Text Is Exist See Text! : " + element);
        } catch (NoSuchElementException ca) {
            logger.writeToLogAndConsole (LogLevel.ERROR, "Assert Failed !" + ca.getMessage ());
        } catch (Exception e) {
            logger.writeToLogAndConsole (LogLevel.ERROR, "Element not Exists :");
        }
    }

    public static boolean verifyElementsExistCatch (WebElement element) {
        try {
            System.setProperty("org.uncommons.reportng.escape-output","true");
            logger.writeToLogAndConsole (LogLevel.DEBUG,"Verify If Element Is Presented On Screen");
            Reporter.log ("Verify If Element Is Presented On Screen");
            if(element.isDisplayed() | element.isEnabled () | element.isSelected ()){
                Reporter.log ("Element Is Displayed As : " + element.getText ());
                logger.writeToLogAndConsole (LogLevel.INFO, "Element Is Displayed" + element.getText ());
                element.getTagName ();
                return true;
            }else{
                logger.writeToLogAndConsole (LogLevel.DEBUG, "Element is not Displayed");
                return false;
            }
        }catch(NoSuchElementException | TimeoutException e){
            logger.writeToLogAndConsole (LogLevel.ERROR, "Verify is Failed : " + e.getMessage ());
            try {
                Reporter.log ("Verify is Failed : " + e.getMessage () + BaseUtility.takeScreenShot ());
            }catch (IOException i) {
                i.getCause ();
            }
        }
        return true;
    }

    public static <T,V>boolean verfyAsserts(boolean ifTrueThanAssertEqualsIfFalseThanAssertNotEquals,T element, V expectedValue) {
        assert softAssert == null;
        try {
            if (ifTrueThanAssertEqualsIfFalseThanAssertNotEquals) {
                logger.writeToLogAndConsole (LogLevel.INFO, "verify if element -> " + element + "is equals to -> " + expectedValue);
                softAssert.assertEquals (element, expectedValue);
            }else {
                softAssert.assertNotEquals(element,expectedValue);
                logger.writeToLogAndConsole (LogLevel.INFO, "verify if element -> " + element + "is not equals to -> " + expectedValue);
            }
        } catch (Exception e) {
            logger.writeToLogAndConsole (LogLevel.ERROR, "Assert Failed !" + e.getMessage ());
        }
        return true;
    }

    public static boolean ifElementCompereTextExists (WebElement element, String expectedValue) {
        try {
            if (element.getText ().equals (expectedValue) || element.getText ().contains (element.getText ())) {
                logger.writeToLogAndConsole (LogLevel.DEBUG, "this element is : " + element.getText ());
            }
        } catch (Exception e) {
            logger.writeToLogAndConsole (LogLevel.ERROR, "See Exception : " + e.getMessage ());
        }
        return true;
    }


    public static void waitForElement(WebElement element, Integer timeout) {
        FluentWait elementWaiter = new FluentWait (Base.driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        elementWaiter.until(ExpectedConditions.visibilityOf(element));


    }

    public static boolean isElementPresented(WebElement w, int timeOut) {

        boolean isElementPresented = false;

        try {
            waitForElement(w, timeOut);
            if (w.isDisplayed()) {
                isElementPresented = true;
                return isElementPresented;
            }
        } catch (Exception e) {
            System.out.println(" No Such Element found here - details below:");
            System.out.println("" + e.getMessage());
            isElementPresented = false;
            return isElementPresented;
        }

        return isElementPresented;
    }

}
