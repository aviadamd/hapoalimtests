package helper.wait;

public enum WaitStrategy {
	/**
	 * Wait until element is enabled.
	 */
	ENABLED,
	/**
	 * Wait until element is present.
	 */
	PRESENT,
	/**
	 * Wait until element is displayed.
	 */
	VISIBLE
}