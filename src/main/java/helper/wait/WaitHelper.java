package helper.wait;

import core.base.Base;
import helper.actions.MyScreenRecorder;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.functions.ExpectedCondition;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;

import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

/**
 * @author Aviad Ben Shemon
 */
public class WaitHelper extends Base {
	
	private static WebDriver driver;
	private static Log logger = new Log ();
	private static WebDriverWait wait = null;

	
	public WaitHelper(WebDriver driver) { this.driver = driver; }


	/**
	 *
	 * @param element
	 * @param waitStrategy
	 * @param <T>
	 * @param <V>
	 */
	public static <T,V> void waitFor(final WebElement element, final WaitStrategy waitStrategy) {
		try {
			switch (waitStrategy) {
				case ENABLED:
					wait.until(ExpectedConditions.elementToBeClickable(element));
					break;
				case PRESENT:
					wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy ((By) element));
					break;
				case VISIBLE:
				default:
					wait.until(visibilityOfAllElementsLocatedBy ((By) element));
					break;
			}
		}catch (NullPointerException n){
			logger.writeToLogAndConsole (LogLevel.ERROR, n.getCause());
		}
	}

	/**
	 *
	 * @param locator
	 * @param waitStrategy
	 */
	public static void waitFor(final By locator, final WaitStrategy waitStrategy) {
		switch (waitStrategy) {
			case ENABLED:
				wait.until(ExpectedConditions.elementToBeClickable(locator));
				break;
			case PRESENT:
				wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
				break;
			case VISIBLE:
			default:
				wait.until(visibilityOfAllElementsLocatedBy(locator));
				break;
		}
	}




	private static WebDriverWait getWait (int timeOutInSeconds, int pollingEveryInMiliSec) {
		WebDriverWait wait = new WebDriverWait (driver, timeOutInSeconds);
		wait.pollingEvery (pollingEveryInMiliSec,TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(NoSuchFrameException.class);
		return wait;
	}


	public static boolean waitForElementVisible(WebElement locator, int timeOutInSeconds, int pollingEveryInMiliSec) {
		try {
			logger.writeToLogAndConsole (LogLevel.DEBUG, locator);
			WebDriverWait wait = getWait (timeOutInSeconds, pollingEveryInMiliSec);
			wait.until (ExpectedConditions.visibilityOf (locator));
			return true;
		}catch (NullPointerException n){
			n.getMessage ();
			return false;
		}catch (NoSuchElementException n){
			n.getMessage ();
		}
		return true;
	}

}
