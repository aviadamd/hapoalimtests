package helper.generichelper;


import io.appium.java_client.AppiumDriver;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;


/**
 * @author Aviad Ben Shemon
 */

public class GenericHelper{
	
	private static Logger log = Logger.getLogger(GenericHelper.class);


	public String readValueFromInput(WebElement element) {
		if (null == element){
			return null;
		}
		if (!isDisplayed(element)){
			return null;
		}
		String value = element.getAttribute("value");
		log.info(value);
		return value;
	}
	
	public boolean isDisplayed(WebElement element) {
		try {
			if (element.isDisplayed ())
			//element.isDisplayed();
			log.info("element is displayed.."+element);
			return true;
		} catch (Exception e)
		{
			log.info(e);
			Reporter.log(e.fillInStackTrace().toString());
			return false;
		}
	}
	
	protected boolean isNotDisplayed(WebElement element) {
		try {
			element.isDisplayed();
			log.info("element is displayed.."+element);
			return false;
		} catch (Exception e) {
			log.error(e);
			Reporter.log(e.fillInStackTrace().toString());
			return true;
		}
	}
	
	protected String getDisplayText(WebElement element, WebElement webElement)
	{
		if (null == element){
			return null;
		}
		if (!isDisplayed(element)){
			return null;
		}
		String Result = element.getText ();
		if (Result == null) {
			return Result;
		}
		return element.getText ();
	}
	

	public static String getElementText( WebElement element)
	{
		if (null == element)
		{
			log.info("weblement is null");
			return null;
		}
		String elementText = null;
		try {
			elementText = element.getText();
		} catch (Exception ex) {
			log.info("Element not found " + ex);
			Reporter.log(ex.fillInStackTrace().toString());
		}
		return elementText;
	}

	public boolean isElementPresent(AppiumDriver<WebElement> driver, By locatorKey) {
		try {
			log.info ("Check If Element Is Exist : " + WebElement.class);
			driver.findElement (locatorKey);
			return true;
		} catch (NoSuchElementException e) {
			// e.printStackTrace();
			return false;
		}
	}


}
