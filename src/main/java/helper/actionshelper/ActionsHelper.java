package helper.actionshelper;

import core.base.Base;
import helper.alert.AlertHelper;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;

public class ActionsHelper extends Base
{

    WebDriver driver = Base.driver;
    Actions actions = new Actions (driver);
    private Log logger = new Log ();


    public ActionsHelper (){
        logger.writeToLogAndConsole(LogLevel.INFO,"AlertHelper : " + actions + driver.hashCode());
    }

    public Actions getActions(){

        return getActions ();
    }

    public void ClickById(WebDriver Idriver, String Iid){

        getActions().moveToElement (Idriver.findElement (By.id (Iid))).click ().build ().perform ();
    }

    public void ClickByXpath(WebDriver Idriver, String Xxpath){
        getActions().moveToElement (Idriver.findElement(By.xpath (Xxpath))).click ().build ().perform ();
    }

    public void ClickByClassName(WebDriver Idriver, String CclassName){
        getActions().moveToElement (Idriver.findElement(By.className (CclassName))).click ().build ().perform ();
    }

    public void ClickOnWebElement(WebElement element)
    {
        getActions().click (element);
    }

    public void MoveToElementByOffSet(WebElement element,int x,int y){
        getActions().moveToElement (element,x,y).click ();
    }

    public void sendKeysAction(WebElement element)
    {
        getActions().sendKeys (element);
    }

    public void sendKeysTo(WebElement element,CharSequence charSequence){
        getActions().sendKeys (element, charSequence);
    }

    public void clickAndHold(WebElement element)
    {
        getActions().clickAndHold (element);
    }

    public void doubleClick(WebElement element)
    {
        getActions().doubleClick (element);
    }


}
