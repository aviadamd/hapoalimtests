
package utilities.timer;

import org.joda.time.DateTime;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;


public class DateAndTime{


	private static Date date;
	private static DateTime datetime;

	DateAndTime(){ date = new Date (); }


	public static String getTimeStamp (){

		String timeStamp = new SimpleDateFormat ("yyyy-MM-dd - HH-mm-ss")
				.format (Calendar.getInstance ().getTime ());
		return timeStamp;
	}

	public static int getRandomNumber(){
		Random rand = new Random();
		int next;
		next = rand.nextInt(1000) + 1000;
		return next;
	}

	/*	To get the Current Time */
	public static String getTime(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("kk.mm");
		String TimeNow = dateFormat.format(date);
		return TimeNow;
	}

	/*	To get the Current Date */
	public static String getDate(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String DateNow = dateFormat.format(date);
		return DateNow;
	}

	/*	To get the Current Month in Integer */
	public static int getMonth_Integer() {
		datetime = DateTime.now();
		int month = datetime.getMonthOfYear();
		return month;
	}
	
	/*	To get the Current Month Text as Full in String*/
	public static String getMonth_Full() {
		datetime = DateTime.now();
		String month_Full = datetime.monthOfYear().getAsText();
		return month_Full;
	}
	
	/*	To get the Current Month Text as Short in String*/
	public static String getMonth_Short() {
		datetime = DateTime.now();
		String month_Short = datetime.monthOfYear().getAsShortText();
		return month_Short;
	}
	
	/*	To get the Current Day of the Month */
	public static String getDayOfTheMonth(){
		datetime = DateTime.now();
		String dayOfTheMonth = datetime.dayOfMonth().getAsText();
		return dayOfTheMonth;
	}
	
	/*	To get the Current Day Count in the Year */
	public static String getDayCount(){
		datetime = DateTime.now();
		String dayCountYear = datetime.dayOfYear().getAsText();
		return dayCountYear;
	}
	
	/*	To get the Current Minute of the Hour in String */
	public static String getMinuteOfTheHourAsString(){
		datetime = DateTime.now();
		String minuteOfTheHour = datetime.minuteOfHour().getAsText();
		return minuteOfTheHour;
	}
	
	/*	To get the Current Year as Integer */
	public static int getYear(){
		datetime = DateTime.now();
		int year = datetime.getYear();
		return year;
	}
	
	/*	To get the Current Hour of the Day in String */
	public static String getHourOfTheDay(){
		datetime = DateTime.now();
		String hour = datetime.hourOfDay().getAsShortText();
		return hour;
	}
	
	/*	To get the Current Week Count */
	public static String getWeekCount(){
		datetime = DateTime.now();
		String hour = datetime.weekOfWeekyear().getAsText();
		return hour;
	}

}
