package utilities.timer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Timer
{
    private long startStamp;

    public void start()
    {
        startStamp = getTimeStamp();
    }

    public static long getTimeStamp ()
    {
        return new Date().getTime ();
    }

    public boolean expired(int seconds){
        int difference = (int) ((getTimeStamp() - startStamp) / 1000);
        return difference > seconds;
    }

    public static int getDifference(long start, long end)
    {
        return (int) ((end - start)/1000);
    }


    public static void waitMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.getMessage ();
        }
    }

    private static void waitSeconds (int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.getMessage ();
        }
    }
}

