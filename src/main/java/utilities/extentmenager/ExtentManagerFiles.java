package utilities.extentmenager;

/**
 * Files Paths
 */
public interface ExtentManagerFiles {

    String reportFileName = "/target/surefire-reports/html/extent_report.html";
    String macPath = System.getProperty("user.dir");
    String windowsPath = System.getProperty("user.dir");
    String macReportFileLoc = macPath + "/" + reportFileName;
    String winReportFileLoc = windowsPath + reportFileName;
}
