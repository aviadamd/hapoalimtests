package utilities.extentmenager;



import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.ExtentTestInterruptedException;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.commons.lang3.ObjectUtils;
import org.openqa.selenium.Platform;
import org.testng.annotations.Test;
import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;
import java.io.File;
import java.util.HashMap;
import java.util.Map;



public class ExtentsManager{

    /**
     * CREATE BY AVIAD AT 22:30 AT 24/12/2018
     * SIMPLE CLASS AND METHODS USING EXTENT REPORT VERSION 3.1.5
     * Helper : http://extentreports.com/docs/versions/3/java/
     * All Methods Are Static No Need For Instance An Object From This Class
     */

    private static ExtentReports extent;
    private static Platform platform;
    private static Log logger = new Log ();
    private static ThreadLocal<ExtentTest> test = new ThreadLocal<>();
    private final static Map extentTestMap = new HashMap ();



    /**
     * if extent report is session
     * is null than its will instance createInstance method
     * and start extent report instance
     * @return
     */
    public static ExtentReports getInstance() {
        try{
        if (extent == null)
            createInstance ();

        }catch(NullPointerException instance){
            logger.writeToLogAndConsole(LogLevel.ERROR,instance.getStackTrace());
            logger.writeToLogAndConsole(LogLevel.ERROR,instance.getMessage());
        }catch(ExceptionInInitializerError exceptionInInitializerError){
            logger.writeToLogAndConsole(LogLevel.ERROR,exceptionInInitializerError.getCause().getMessage());
        }catch(ExtentTestInterruptedException extentTestInterruptedException){
            logger.writeToLogAndConsole(LogLevel.ERROR,extentTestInterruptedException.getMessage());
        }
        return extent;
    }

    /**
     * call by static can use for log reports
     * and for screen shots .................
     * @return
     */
    public static synchronized ExtentTest getTest() {
        return (ExtentTest)extentTestMap.get((int) (long) (Thread.currentThread().getId()));
    }


    /**
     * referring call to getCurrentPlatform within a platform enum
     * call to getReportFileLocation inside a
     * initiate from ExtentHtmlReporter
     * new object with the file name as string
     * chose the enum ChartLocation.BOTTOM to get The Chart the at the bottom also can be chosen as ChartLocation.Up
     * chose the enum Theme.STANDARD or Theme.DARK will display the extent report in dark mode or standard
     * initiate new object from ExtentReport
     * with htmlReporter Object
     * private function belong to this class and in getInstance method
     * @return
     */
    private static ExtentReports createInstance (){
        platform = getCurrentPlatform();
        final String extentFile;
        extentFile = getReportFileLocation(platform);
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter (extentFile);
        htmlReporter.config ().setTestViewChartLocation(ChartLocation.BOTTOM);
        htmlReporter.config ().setChartVisibilityOnOpen(true);
        htmlReporter.config ().setTheme(Theme.STANDARD);
        htmlReporter.config ().setDocumentTitle(extentFile);
        htmlReporter.config ().setEncoding("utf-8");
        htmlReporter.config ().setReportName(extentFile);
        htmlReporter.config ().getProtocol();
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        return extent;
    }


    /**
     * Select the extent report file
     * location based on platform whatever
     * if its mac or windows it will get
     * the extent xml file location and use
     * the html tags to create beautiful report
     * @param platform
     * @return
     */
    private static String getReportFileLocation (Platform platform) {
        String reportFileLocation = null;
        switch (platform) {
            case MAC:
                reportFileLocation = ExtentManagerFiles.macReportFileLoc;
                createReportPath(ExtentManagerFiles.macPath);
                logger.writeToLogAndConsole(LogLevel.DEBUG,"ExtentReport File Location Path for MAC: " + ExtentManagerFiles.macPath + "\n");
                break;
            case WINDOWS:
                reportFileLocation = ExtentManagerFiles.winReportFileLoc;
                createReportPath(ExtentManagerFiles.windowsPath);
                createReportPath("C:\\Users\\aviad\\Desktop");
                logger.writeToLogAndConsole(LogLevel.DEBUG,"ExtentReport File Location Path for WINDOWS:"+ ExtentManagerFiles.windowsPath + "\n");
                break;
            default:
                logger.writeToLogAndConsole(LogLevel.DEBUG,"ExtentReport File Location Path has not been set! There is a problem!\n");
                break;
        }
        return reportFileLocation;
    }


    /**
     * crate report file if
     * the file does not exist
     * after calling this function
     * give it the string path
     * for create a new extent file
     * @param extentFilePath
     */
    private static void createReportPath(String extentFilePath){
        File testDirectory = new File(extentFilePath);
        try {
            if (!testDirectory.exists()) {
                if (testDirectory.mkdir()) {
                    logger.writeToLogAndConsole(LogLevel.INFO, "Directory: " + extentFilePath + " is created!" );
                } else {
                    logger.writeToLogAndConsole(LogLevel.DEBUG, "Failed to create directory: " + extentFilePath);
                }
            } else {
                logger.writeToLogAndConsole(LogLevel.ERROR, "Extent Report Is Directory already exists At: " + extentFilePath);
            }
        }catch (Exception n){
            n.getMessage ();
        }

    }



    /**
     * here you will get
     * the current platform
     * whatever if its window
     * mac or linux operation
     * @return
     */
    private static Platform getCurrentPlatform (){
        if (platform == null) {
            String operationSystem = System.getProperty("os.name").toLowerCase();
            if (operationSystem.contains("win")) {
                platform = Platform.WINDOWS;
            } else if (operationSystem.contains("nix") || operationSystem.contains("nux")
                    || operationSystem.contains("aix")) {
                platform = Platform.LINUX;
            } else if (operationSystem.contains("mac")) {
                platform = Platform.MAC;
            }
        }
        return platform;
    }
}