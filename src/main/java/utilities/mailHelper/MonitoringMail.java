package utilities.mailHelper;

import utilities.customlogger.Log;
import utilities.customlogger.LogLevel;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Properties;


public class MonitoringMail{

	private Log logger = new Log ();

	public void sendMail(String mailServer, String from, String[] to, String subject, String messageBody){
		try {
			boolean debug = true;
			Properties props = new Properties ();
			props.put ("mail.smtp.starttls.enable", "true");
			props.put ("mail.smtp.EnableSSL.enable", "true");
			props.put ("mail.smtp.auth", "true");
			props.put ("mail.smtp.host", mailServer);
			props.put ("mail.debug", "true");
			props.setProperty ("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.setProperty ("mail.smtp.socketFactory.fallback", "false");
			props.setProperty ("mail.smtp.port", "465");
			props.setProperty ("mail.smtp.socketFactory.port", "465");
			Authenticator auth = new SMTPAuthenticator ();
			Session session = Session.getDefaultInstance (props, auth);
			session.setDebug (debug);
			 Transport bus = session.getTransport("smtp");
			 bus.connect();
             Message message = new MimeMessage(session);
         //X-Priority values are generally numbers like 1 (for highest priority), 3 (normal) and 5 (lowest).
             message.addHeader("X-Priority", "1");
             message.setFrom(new InternetAddress(from));
             InternetAddress[] addressTo = new InternetAddress[to.length];
             for (int i = 0; i < to.length; i++)
      		 addressTo[i] = new InternetAddress(to[i]);
             message.setRecipients(Message.RecipientType .TO, addressTo);
             message.setSubject(subject);
             BodyPart body = new MimeBodyPart();
             body.setContent(messageBody,"text/html");
             MimeMultipart multipart = new MimeMultipart();
             multipart.addBodyPart(body);
             message.setContent(multipart);
             Transport.send(message);
             logger.writeToLogAndConsole (LogLevel.INFO,"Successfully Sent mail to All Users");
         	 bus.close();
		} catch(MessagingException mex){
            mex.printStackTrace();
        }
	} 
	
	private class SMTPAuthenticator extends Authenticator{

	    public PasswordAuthentication getPasswordAuthentication(){
	        String username = MailTestConfig.from;
	        String password = MailTestConfig.password;
	        return new PasswordAuthentication(username, password);
	    }
	}
}
