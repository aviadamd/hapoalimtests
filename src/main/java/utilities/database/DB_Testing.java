package utilities.database;

import com.mongodb.DB;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DB_Testing
{
    WebDriver driver;

    @Test
    public void selectDBdata() throws ClassNotFoundException, SQLException{

        DataBase dataBase = new DataBase();
        ResultSet data = dataBase.getData(DBCredentials.query);
        System.out.println(data);
        while(data.next()){
            System.out.println(data.getString(1)+" "
                    +data.getString(2)+" "+data.getString(3));
        }
    }

    @Test
    public void insertDBdata() throws ClassNotFoundException, SQLException{
        DataBase dataBase = new DataBase();
        dataBase.insertData(DBCredentials.InsertQuery);
    }

    @Test
    public void updateDBdata() throws ClassNotFoundException, SQLException{

        DataBase dataBase = new DataBase();
        dataBase.updateData(DBCredentials.UpdateQuery);

    }

}
