
package utilities.customlogger;

public enum LogLevel {

	/**
	 * Debug.
	 */
	DEBUG (" [DEBUG]"),
	/**
	 * Debug+Debug
	 */
	DEBUG_DEBUG (" [DEBUG:DEBUG]"),
	/**
	 * Debug+Error
	 */
	DEBUG_ERROR (" [DEBUG:ERROR]"),
	/**
	 * Debug+Info
	 */
	DEBUG_INFO (" [DEBUG:INFO]"),
	/**
	 * Debug+Warn
	 */
	DEBUG_WARN (" [DEBUG:WARN]"),
	/**
	 * Error
	 */
	ERROR (" [ERROR]"),
	/**
	 * Error+Debug
	 */
	ERROR_DEBUG (" [ERROR:DEBUG]"),
	/**
	 * Error+Error
	 */
	ERROR_ERROR (" [ERROR:ERROR]"),
	/**
	 * Error+Info
	 */
	ERROR_INFO (" [ERROR:INFO]"),
	/**
	 * Error+Warn
	 */
	ERROR_WARN (" [ERROR:WARN]"),
	/**
	 * Info
	 */
	INFO (" [INFO]"),
	/**
	 * Info+Debug
	 */
	INFO_DEBUG (" [INFO:DEBUG]"),
	/**
	 * Info+Error
	 */
	INFO_ERROR (" [INFO:ERROR]"),
	/**
	 * Info+Info
	 */
	INFO_INFO (" [INFO:INFO]"),
	/**
	 * Info+Warn
	 */
	INFO_WARN (" [INFO:WARN]"),
	/**
	 * Warn
	 */
	WARN (" [WARN]"),
	/**
	 * Warn+Debug
	 */
	WARN_DEBUG (" [WARN:DEBUG]"),
	/**
	 * Warn+Error
	 */
	WARN_ERROR (" [WARN:ERROR]"),
	/**
	 * Warn+Info
	 */
	WARN_INFO (" [WARN:INFO]"),
	/**
	 * Warn+Warn
	 */
	WARN_WARN (" [WARN:WARN]");

	private final String level;

	LogLevel(final String level){
		this.level = level;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString(){
		return this.level;
	}
}