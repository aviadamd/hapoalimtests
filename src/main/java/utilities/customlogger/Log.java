package utilities.customlogger;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;



public class Log implements Serializable{


    public final static PrintStream out = null;


    public Log () {
    }

    private String getTimeStamp () {
        return new SimpleDateFormat ("yyyy-MM-dd-HH-mm-ss").format (Calendar.getInstance ().getTime ());
    }


    public void writeToConsole (LogLevel level, Object o) {
        try {
            System.out.println (getTimeStamp () + level + o);
        } catch (Exception e) {
            e.getMessage ();
            e.getStackTrace ();
        }
    }

    public Object writeToLog (LogLevel logLevel, Object o) {
        Object t = writeToLogFile (getTimeStamp () + logLevel + o + "\n");
        return t;
    }

    public void writeToLogAndConsole (LogLevel logLevel, Object o) {
        try {
            writeToLog (logLevel, o);
            writeToConsole (logLevel, o);
        } catch (Exception e) {
            e.getMessage ();
        }
    }

    private String createDirectory (String dir) {
        File file = new File (dir);
        if (!file.exists ()) {
            if (file.mkdir ()) {
                System.out.println ("A New Directory Is Created For Log Reports");
            } else {
                System.out.println ("Failed To Created New Directory Reports");
            }
        }
        return dir;
    }







    public static void showArrayList (ArrayList<Object> list) {
        if(!list.isEmpty()){
            list.forEach(System.out::println); //-> go direct to super
        }else
            System.out.println("list is empty");
    }



    private Object writeToLogFile(String writeData) {
        BufferedWriter bufferedWriter = null;
        FileWriter fileWriter = null;
        createDirectory(FileLocation.LOGGER_DIRCTORY_PATH);
        try{
            File file = new File (FileLocation.LOG_PATH);
            if(!file.exists()){
                final boolean newFile = file.createNewFile ();
                if (!newFile) file.delete ();
                System.out.println ("There Is No Logger File About To Create A New One");
            }
            fileWriter = new FileWriter (file.getAbsoluteFile (), true);
            bufferedWriter = new BufferedWriter (fileWriter);
            bufferedWriter.write(writeData);
        }catch(Exception e){ e.printStackTrace ();}

        finally{
            try{
                if(bufferedWriter != null)
                    bufferedWriter.close ();
                if(fileWriter != null)
                    fileWriter.close ();
            }catch(IOException io) {
                io.printStackTrace ();}
             catch(NullPointerException n){
                n.getMessage ();}
        }
        return null;
    }
}










