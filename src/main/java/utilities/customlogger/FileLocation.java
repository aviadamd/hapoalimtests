package utilities.customlogger;

public interface FileLocation {

    String LOGGER_DIRCTORY_PATH = System.getProperty ("user.dir") + "/LoggerReport";
    String LOG_PATH = LOGGER_DIRCTORY_PATH + "/report.log";
}
