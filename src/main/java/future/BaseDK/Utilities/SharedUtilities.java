//package future.BaseDK.Utilities;
//
//
//import core.base.Base;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Dimension;
//import org.openqa.selenium.NoSuchElementException;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.FluentWait;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import java.io.IOException;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.android.AndroidKeyCode;
//
//
//public class SharedUtilities {
//
//    public static AppiumDriver appiumDriver = (AppiumDriver) Base.driver;
//    public static Dimension size = Base.driver.manage().window().getSize();
//
//    // Wait for element functions
//
//    public static void elementToBeClickable(WebElement webElement) {
//
//        WebDriverWait wait = new WebDriverWait(appiumDriver, 20);
//
//        try {
//            wait.until(ExpectedConditions.elementToBeClickable(webElement));
//            System.out.println(webElement + " is available for click");
//        } catch (Exception e) {
//            System.out.println(webElement + " is not available for click");
//        }
//    }
//
//
//    public static void waitForElement(WebElement element, Integer timeout) {
//        FluentWait elementWaiter = new FluentWait(Base.driver)
//                .withTimeout(timeout, TimeUnit.SECONDS)
//                .pollingEvery(1, TimeUnit.SECONDS)
//                .ignoring(NoSuchElementException.class);
//
//        elementWaiter.until(ExpectedConditions.visibilityOf(element));
//
//
//    }
//
//
//    public static void waitFewSeconds(Integer seconds) {
//
//
//        try {
//            Thread.sleep(seconds);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public static boolean isElementPresented(WebElement w, int timeOut) {
//
//        boolean isElementPresented = false;
//
//        try {
//            waitForElement(w, timeOut);
//            if (w.isDisplayed()) {
//                isElementPresented = true;
//                return isElementPresented;
//            }
//        } catch (Exception e) {
//            System.out.println(" No Such Element found here - details below:");
//            System.out.println("" + e.getMessage().toString());
//            isElementPresented = false;
//            return isElementPresented;
//        }
//
//        return isElementPresented;
//    }
//
//
//    // Touch Actions - Scroll, Swipe and more
//
////
////    public static void scrollDown() {
////
////        TouchAction touchAction = new TouchAction((AppiumDriver) BasicTest.driver);
////        touchAction.press(1000, size.getHeight() / 2).waitAction(Duration.ofMillis(200)).moveTo(1000, 200).release().perform();
////    }
////
////
////    public static void swipeGestureFromLeftToRight(int timesToPerformSwipe) {
////        for (int i = 0; i < timesToPerformSwipe; i++) {
////            try {
////
////                TouchAction touchAction = new TouchAction((AppiumDriver) BasicTest.driver);
////                touchAction.press(100, size.getHeight() / 2).waitAction(Duration.ofMillis(1000)).moveTo(1000, size.getHeight() / 2).release().perform();
////            } catch (Exception e) {
////                System.out.println("OMG - Couldn't swipe any more");
////                i = timesToPerformSwipe;
////                System.out.println("Done Swiping for " + i + " times");
////            }
////        }
////    }
////
////
////    public static void scrollUp() {
////
////        TouchAction touchAction = new TouchAction((AppiumDriver) BasicTest.driver);
////        touchAction.press(1000, size.getHeight() / 2).waitAction(Duration.ofMillis(200)).moveTo(1000, 1500).release().perform();
////    }
////
////    public static void longPressElement(WebElement webElement) {
////
////        TouchAction touchAction = new TouchAction((AppiumDriver) BasicTest.driver);
////        touchAction.longPress(webElement).release().perform();
////
////    }
//
//
//    // Driver Actions
//
//    public static void closeKeyBoard() {
//        appiumDriver.hideKeyboard();
//    }
//
//    public static List<WebElement> getWebElementsList(By by) {
//
//        return Base.driver.findElements(by);
//    }
//
//
//    public static void backButtonAndroid() {
//
//        appiumDriver.navigate().back();
//
//    }
//
//
//    public static void closeApp() {
//
//        appiumDriver.closeApp();
//
//
//    }
//
//    public static void launchApp() {
//
//        appiumDriver.launchApp();
//
//    }
//
//    public static void clearCache() {
//
//        appiumDriver.resetApp();
//
//    }
//
//    // Just for android
//    public static void enterAndroidButton() {
//
//        AndroidDriver androidDriver = (AndroidDriver) BasicTest.driver;
//        androidDriver.pressKeyCode(AndroidKeyCode.ENTER);
//
//
//    }
//
//    // Just for android
//    public static void homeAndroidButton() {
//
//        AndroidDriver androidDriver = (AndroidDriver) BasicTest.driver;
//        androidDriver.pressKeyCode(AndroidKeyCode.HOME);
//    }
//
//
//    public static void setTextInTextField(String text, WebElement element) {
//        element.clear();
//        CharSequence seq = text;
//        element.sendKeys(seq);
//    }
//
//    public static void clearTextInTextField(WebElement element) {
//
//        element.clear();
//    }
//
////
////    public static void scrollingSeveralTimes(int severalTimes) {
////
////        for (int i = 0; i < severalTimes; i++) {
////
////            SharedUtilities.scrollDown();
////
////        }
////    }
//
//    // Just for android
//    public static void scrollByText(String byText) {
//
//        try {
//            AndroidDriver androidDriver = (AndroidDriver) BasicTest.driver;
//            androidDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"" + byText + "\"));");
//        } catch (Exception e) {
//
//            waitFewSeconds(3000);
//
//        }
//
//    }
//
//
//    public static void addFingerPrint(String fingerNumber) {
//
//
//        try {
//            Runtime.getRuntime().exec("adb devices to get emulator id -->emulator-5554");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        try {
//            Runtime.getRuntime().exec("adb -s emulator-5554 emu finger touch " + fingerNumber);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public static void fingerPrintAuthentication(String fingerNumber) {
//
//        try {
//            SharedUtilities.waitFewSeconds(2000);
//            Runtime.getRuntime().exec("adb -e emu finger touch " + fingerNumber);
//        } catch (IOException e) {
//            SharedUtilities.waitFewSeconds(5000);
//        }
//    }
//
////
////    public static void clickOnAllItemes(WebElement webElement) {
////
////        List<WebElement> webElements = null;
////        //A list of all EditText fields on the screen
////        webElements = SharedUtilities.getListOfItems(webElement);
////        for (int i = 0; i < webElements.size(); ) {
////            if (SharedUtilities.isElementPresented(webElements.get(i), 10)) {
////                webElements.get(i).click();
////                i++;
////            } else {
////
////                break;
////
////            }
////        }
////    }
////
////
////    public static void clickFromSpecificItemByWebElement(int value, WebElement webElement) {
////
////        List<WebElement> webElements = null;
////        //A list of all EditText fields on the screen
////        webElements = SharedUtilities.getListOfItems(webElement);
////        for (int i = value; i < webElements.size(); ) {
////            if (SharedUtilities.isElementPresented(webElements.get(i), 10)) {
////                webElements.get(i).click();
////                i++;
////            } else {
////
////                break;
////
////            }
////        }
////    }
//
//
//    public static void clickOnSpecificItemByClassName(String byClass, int option) {
//
//        BasicTest.driver.findElements(By.className(byClass)).get(option).click();
//
//    }
//
//    public static void clickOnSpecificItemByXpath(String byXpath, int option) {
//
//        BasicTest.driver.findElements(By.xpath(byXpath)).get(option).click();
//
//    }
//
//
//    // Just for android
//    public static void getCurrentActivity(){
//
//        AndroidDriver androidDriver = (AndroidDriver) BasicTest.driver;
//        System.out.println(androidDriver.currentActivity());
//
//    }
//
////
////    public static List<WebElement> getListOfItems(WebElement webElement) {
////        ReadFromJsonFile readFromJsonFile = new ReadFromJsonFile();
////        List<WebElement> elements = new ArrayList<>();
////        if (readFromJsonFile.getPlatformType().equals(Constants.ANDROID_PLATFORM)) {
////
////            String idOfElement = getIdOfWebElementForAndroid(webElement);
////            elements.addAll(BasicTest.driver.findElements(By.id(idOfElement)));
////        } else {
////            // IOS
////            String cellIdentifier = webElement.getAttribute("name");
////            elements.addAll(BasicTest.driver.findElements(By.id(cellIdentifier)));
////        }
////        return elements;
////    }
////
////    // Just for android
////    public static String getIdOfWebElementForAndroid(WebElement webElementForAndroid) {
////        String retVal = "";
////        try {
////            retVal = webElementForAndroid.toString().split(Constants.ANDROID_ID)[1];
////            retVal = retVal.split(" \n")[0];
////            retVal = retVal.split("\"")[0];
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        return retVal;
////    }
//
////    public static void androidFindElementsByAndClick(By by, String elementText) {
////        List<WebElement> menuElements = BasicTest.driver.findElements(by);
////        WebElement requestedElement = null;
////        for (int i = 0; i < menuElements.size(); i++) {
////            String text = menuElements.get(i).getAttribute("text");
////            if (text.equals(elementText)) {
////                requestedElement = menuElements.get(i);
////                //the right item was found, no need to run the loop any more
////                i = 222;
////            }
////        }
////        SharedUtilities.waitForElement(requestedElement, Constants.TIMEOUT);
////        requestedElement.click();
////    }
//
//
//    public static void CloseKeyBoard() {
//        appiumDriver.hideKeyboard();
//    }
//
//
//}
//
//
//
//
//
