//package future.BaseDK.BasicClass;
//
//
//import configs.ReadFromJsonFile;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import java.net.MalformedURLException;
//import java.net.URL;
//import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.ios.IOSDriver;
//import io.appium.java_client.remote.MobileCapabilityType;
//
//
//public class BasicTest {
//
//    public static WebDriver driver;
//    private ReadFromJsonFile readFromJsonFile = new ReadFromJsonFile();
//    private DesiredCapabilities capabilities = new DesiredCapabilities();
//
//
//    public BasicTest(){
//        readFromJsonFile.readFromPropertiesJson();
//        if(readFromJsonFile.getPlatformType().equals("ANDROID")){
//            System.out.println("PLATFORM_TYPE = Android");
//            initiateAndroidDriver();
//        }else{
//            System.out.println("PLATFORM_TYPE = IOS");
//            initiateIosDriver();
//        }
//    }
//
//    private WebDriver initiateAndroidDriver(){
//
//        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
//        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "device");
//        capabilities.setCapability(MobileCapabilityType.APP,"");
//        capabilities.setCapability(MobileCapabilityType.NO_RESET,true);
//        if(driver == null) {
//            try{
//                driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
//            }
//            catch (MalformedURLException e){
//                e.printStackTrace();
//            }
//        }
//        return driver;
//    }
//
//    private WebDriver initiateIosDriver(){
//
//        capabilities.setCapability(MobileCapabilityType.APP,"");
//        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "");
//        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "");
//        capabilities.setCapability(MobileCapabilityType.NO_RESET,true);
//        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8 Plus");
//        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
//
//        if(driver == null) {
//            try {
//                driver = new IOSDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
//
//            } catch (MalformedURLException e) {
//            }
//        }
//
//        return driver;
//    }
//
//
//}
