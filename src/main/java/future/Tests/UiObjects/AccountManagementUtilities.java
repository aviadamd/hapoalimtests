//package future.Tests.UiObjects;
//
//import org.openqa.selenium.By;
//import future.BaseDK.BasicClass.BasicTest;
//import configs.ReadFromJsonFile;
//import future.BaseDK.Utilities.SharedUtilities;
//import General.Constants;
//import Tests.Tests.CurrencyExchangeTests;
//import Tests.Tests.DepositsTests;
//import Tests.Tests.LoginPageTests;
//import Tests.Tests.RequestForLoanTests;
//import Tests.Tests.TransferMoneyTests;
//import static future.BaseDK.Utilities.SharedUtilities.elementToBeClickable;
//
//
//public class AccountManagementUtilities extends BasicTest {
//
//    private ReadFromJsonFile readFromJsonFile = new ReadFromJsonFile();
//    public static AccountManagementUI accountManagementUI;
//
//
//    public AccountManagementUtilities(){
//        accountManagementUI = new AccountManagementUI();
//    }
//
//    public void serverForWork(){
//        //minionsScreen(readFromJsonFile.getServerNameBusiness(),readFromJsonFile.getServerNumberBusiness());
//        minionsScreen(readFromJsonFile.getServerNameAccountManagement(),readFromJsonFile.getServerNumberAccountManagement());
//    }
//
//    public void userForWork(){
//        //selectServer(readFromJsonFile.getUserTypeBusiness());
//        selectServer(readFromJsonFile.getUserTypeAccountManagement());
//    }
//
//
//    public void loginWithTrueValues() {
//        if(readFromJsonFile.getProject().equals(ReadFromJsonFile.accountManagementBusiness)) {
//            LoginPageTests loginPageTests = new LoginPageTests();
//            loginPageTests.A_minionsScreen();
//            loginPageTests.B_installAppFirstTime();
//            loginPageTests.C_pageIsFullyLoaded();
//            loginPageTests.D_loginWithTrueValues();
//        }if(readFromJsonFile.getProject().equals(ReadFromJsonFile.accountManagement)){
//            LoginPageTests loginPageTests = new LoginPageTests();
//            loginPageTests.A_minionsScreen();
//            loginPageTests.B_installAppFirstTime();
//            loginPageTests.C_pageIsFullyLoaded();
//            loginPageTests.D_loginWithTrueValues();
//        }else {
//            System.out.println("Error As Been Accord Cannot Read From Json File");
//        }
//    }
//
//
//        public static void transferMoneyTest(){
//        TransferMoneyTests transferees = new TransferMoneyTests();
//        transferees.B_transferMoneyTestStep1();
//        transferees.C_transferMoneyTestStep2();
//        transferees.D_transferMoneyTestStep3();
//        transferees.E_transferMoneyTestStep4();
//    }
//
//
//    public static void currencyExchangeTests(){
//        CurrencyExchangeTests currencyExchangeTests = new CurrencyExchangeTests();
//        currencyExchangeTests.B_goToCurrencyExchange();
//        currencyExchangeTests.C_currencyExchange_btnNisToForeign();
//        currencyExchangeTests.Ca_currencyExchange_btnForeignToNIS();
//    }
//
//    public static void depositsTests(){
//        DepositsTests depositsTests = new DepositsTests();
//        depositsTests.B_goToActionsInDeposits();
//        depositsTests.C_addToExistingDiposit();
//        depositsTests.D_newDipositTest();
//        depositsTests.E_depositWithdrawl();
//    }
//
//
//    public static void requestForLoanTests(){
//        RequestForLoanTests requestForLoanTests =  new RequestForLoanTests();
//        requestForLoanTests.B_goToRequestForLoan();
//        requestForLoanTests.C_creditLoanStep1();
//        requestForLoanTests.Ca_creditLoanStep2();
//        requestForLoanTests.Cb_creditLoanStep3();
//        requestForLoanTests.Cc_creditLoanStep4();
//    }
//
//    public void goToAllActions(){
//        elementToBeClickable(accountManagementUI.genericLabels.allActions);
//        accountManagementUI.genericLabels.allActions.click();
//        elementToBeClickable(accountManagementUI.genericLabels.pageIdentifier);
//    }
//
//    public void selectServer(String server){
//
//        if(readFromJsonFile.getProject().equals(readFromJsonFile.accountManagement)) {
//            switch (server) {
//                case Constants.MOB1:
//                    SharedUtilities.scrollByText(Constants.MOB1);
//                    accountManagementUI.minionsScreen.mob1.click();
//                    break;
//                case Constants.MOB2:
//                    SharedUtilities.scrollByText(Constants.MOB2);
//                    accountManagementUI.minionsScreen.mob2.click();
//                    break;
//                case Constants.MOB3:
//                    SharedUtilities.scrollByText(Constants.MOB3);
//                    accountManagementUI.minionsScreen.mob3.click();
//                    break;
//                case Constants.PRE_PROD:
//                    SharedUtilities.scrollByText(Constants.PRE_PROD);
//                    accountManagementUI.minionsScreen.preProd.click();
//                    break;
//                case Constants.PROD:
//                    SharedUtilities.scrollByText(Constants.PROD);
//                    accountManagementUI.minionsScreen.prod.click();
//                    break;
//                case Constants.PROD_BETA:
//                    SharedUtilities.scrollByText(Constants.PROD_BETA);
//                    accountManagementUI.minionsScreen.beta.click();
//                    break;
//                default:
//                    SharedUtilities.scrollByText(Constants.MOB3);
//                    accountManagementUI.minionsScreen.mob3.click();
//                    break;
//            }
//        }
//
//        else {
//
//        }
//
//    }
//
//    public void selectUser(String selectedUser) {
//        SharedUtilities.elementToBeClickable(accountManagementUI.loginPageScreen.pageIdentifier);
//        SharedUtilities.elementToBeClickable(accountManagementUI.loginPageScreen.continueButton);
//        SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.userNameField);
//        SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.passwordField);
//
//        switch (selectedUser) {
//            case Constants.SYSTEM_MOB:
//                accountManagementUI.loginPageScreen.userNameField.click();
//                SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.userNameField);
//                accountManagementUI.loginPageScreen.userNameField.sendKeys(Constants.USERNAME_SYSTEM);
//                SharedUtilities.CloseKeyBoard();
//                SharedUtilities.elementToBeClickable(accountManagementUI.loginPageScreen.passwordField);
//                accountManagementUI.loginPageScreen.passwordField.click();
//                SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.passwordField);
//                accountManagementUI.loginPageScreen.passwordField.sendKeys(Constants.PASSWORD_SYSTEM);
//                break;
//            case Constants.PRE_PRODUCTION:
//                accountManagementUI.loginPageScreen.userNameField.click();
//                SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.userNameField);
//                accountManagementUI.loginPageScreen.userNameField.sendKeys(Constants.USERNAME_PRE_PROD);
//                SharedUtilities.CloseKeyBoard();
//                SharedUtilities.elementToBeClickable(accountManagementUI.loginPageScreen.passwordField);
//                accountManagementUI.loginPageScreen.passwordField.click();
//                SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.passwordField);
//                accountManagementUI.loginPageScreen.passwordField.sendKeys(Constants.PASSWORD_PRE_PROD);
//                break;
//            case Constants.PRODUCTION:
//                accountManagementUI.loginPageScreen.userNameField.click();
//                SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.userNameField);
//                accountManagementUI.loginPageScreen.userNameField.sendKeys(Constants.USERNAME_PROD);
//                SharedUtilities.CloseKeyBoard();
//                SharedUtilities.elementToBeClickable(accountManagementUI.loginPageScreen.passwordField);
//                accountManagementUI.loginPageScreen.passwordField.click();
//                SharedUtilities.clearTextInTextField(accountManagementUI.loginPageScreen.passwordField);
//                accountManagementUI.loginPageScreen.passwordField.sendKeys(Constants.PASSWORD_PROD);
//                break;
//
//        }
//
//    }
//
//    public void minionsScreen(String selectServer, String serverNumber) {
//
//        SharedUtilities.elementToBeClickable(accountManagementUI.minionsScreen.minionsOptions);
//        accountManagementUI.minionsScreen.minionsOptions.click();
//        selectServer(selectServer); // Mobbiz1, Mobbiz2, Mobbiz3, PreProd, Prod, Beta
//        accountManagementUI.minionsScreen.serverNumber.clear();
//        accountManagementUI.minionsScreen.serverNumber.sendKeys(serverNumber);
//        accountManagementUI.minionsScreen.btnMinions.click();
//
//    }
//
//
//    public void changeAccountTest(int option1, int option2) {
//
//        if (SharedUtilities.isElementPresented(accountManagementUI.genericLabels.changeAccountPicker, 5)) {
//            //SharedAction.elementToBeClickable(accountManagementUI.genericLabels.changeAccountPicker);
//            accountManagementUI.genericLabels.changeAccountPicker.click();
//            SharedUtilities.elementToBeClickable(accountManagementUI.genericLabels.changeAccountTitle);
//            SharedUtilities.clickOnSpecificItemByClassName(Constants.BY_CLASS, option1);
//            SharedUtilities.elementToBeClickable(accountManagementUI.genericLabels.allActions);
//            SharedUtilities.elementToBeClickable(accountManagementUI.genericLabels.changeAccountPicker);
//            accountManagementUI.genericLabels.changeAccountPicker.click();
//            SharedUtilities.elementToBeClickable(accountManagementUI.genericLabels.changeAccountTitle);
//            SharedUtilities.clickOnSpecificItemByClassName(Constants.BY_CLASS, option2);
//            SharedUtilities.elementToBeClickable(accountManagementUI.genericLabels.allActions);
//
//        }
//    }
//
//
//    public void swipeGestureFromLeftToRight(int severalTimes) {
//
//        if(readFromJsonFile.getDeviceType().equals(Constants.EMULATOR)) {
//            SharedUtilities.swipeGestureFromLeftToRight(severalTimes);
//        }
//        else{
//            // Real Device
//        }
//    }
//
//    public void transferBetweenAccounts() {
//        if (readFromJsonFile.getPlatformType().equals(Constants.ANDROID_PLATFORM)) {
//            SharedUtilities.androidFindElementsByAndClick(By.id("com.bnhp.businessapp:id/actionButton"), "בין חשבונות החברה");
//        } else {
//            // IOS
//        }
//
//    }
//
//
//}
//
//
