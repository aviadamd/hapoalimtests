//package future.Tests.Tests;
//
//import org.junit.FixMethodOrder;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.junit.runners.MethodSorters;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebElement;
//import java.util.List;
//import configs.ReadFromJsonFile;
//import future.BaseDK.Utilities.SharedUtilities;
//import future.BaseDK.Utilities.TestRunner;
//import General.Constants;
//import future.Tests.UiObjects.AccountManagementUtilities;
//import static future.BaseDK.Utilities.SharedUtilities.CloseKeyBoard;
//import static future.BaseDK.Utilities.SharedUtilities.elementToBeClickable;
//import static future.BaseDK.Utilities.SharedUtilities.setTextInTextField;
//import static future.BaseDK.Utilities.SharedUtilities.waitForElement;
//
//
//@RunWith(TestRunner.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//public class TransferMoneyTests extends AccountManagementUtilities {
//
//
//    private ReadFromJsonFile readFromJsonFile = new ReadFromJsonFile();
//
//
//    //Step 1
//    @Test
//    public void A_LoginToApp() {
//
//        loginWithTrueValues();
//
//    }
//
//    @Test
//    public void Ab_GoToAllActions() {
//
//        goToAllActions();
//
//    }
//
//
//    @Test
//    public void B_transferMoneyTestStep1() {
//
//        SharedUtilities.elementToBeClickable(accountManagementUI.transferMoneyScreen.transferMoneyButton);
//        accountManagementUI.transferMoneyScreen.transferMoneyButton.click();
//        //Checking if permission alert is presented
//        try {
//            SharedUtilities.elementToBeClickable(accountManagementUI.loginPageScreen.permissionallowbutton);
//            accountManagementUI.loginPageScreen.permissionallowbutton.click();
//        }
//        catch (Exception e){
//
//            elementToBeClickable(accountManagementUI.transferMoneyScreen.balance);
//        }
//    }
//
//
//    @Test
//    public void C_transferMoneyTestStep2() {
//
//        elementToBeClickable(accountManagementUI.transferMoneyScreen.transferToHom);
//        elementToBeClickable(accountManagementUI.transferMoneyScreen.balance);
//        //For android and IOS devices, different implementation is required (can't provide ID's in android platform)
//        if (readFromJsonFile.getPlatformType().equals(Constants.ANDROID_PLATFORM)) {
//
//            if (readFromJsonFile.getDeviceType().equals(Constants.EMULATOR)) {
//                try {
//
//                    SharedUtilities.elementToBeClickable(accountManagementUI.transferMoneyScreen.beneficiarycontainer);
//                    accountManagementUI.transferMoneyScreen.beneficiarycontainer.click();
//                    SharedUtilities.elementToBeClickable(accountManagementUI.transferMoneyScreen.androidEditText);
//                    accountManagementUI.transferMoneyScreen.androidEditText.sendKeys("500");
//
//                } catch (Exception e) {
//
//                    transferMoney("android.widget.EditText", "Yossi Arar", "12", "170", "5070", "1000");
//
//                }
//            } else {
//                //DEVICE_TYPE == REAL DEVICE)
//                if (readFromJsonFile.getTestingEnvironment().equals(Constants.SYSTEM)) {
//                    transferMoney("android.widget.EditText", "Yossi Arar", "12", "170", "5070", "1000");
//                } else {
//                    // Production account
//                    transferMoney("android.widget.EditText", "Yossi Arar", "0", "99", "4798", "1000");
//                }
//            }
//
//        } else {
//            // IOS
//            waitForElement(accountManagementUI.transferMoneyScreen.branch, 5);
//            setTextInTextField("Yossi Arar", accountManagementUI.transferMoneyScreen.transferToHom);
//            accountManagementUI.transferMoneyScreen.bank.clear();
//            setTextInTextField("11", accountManagementUI.transferMoneyScreen.bank);
//            setTextInTextField("92", accountManagementUI.transferMoneyScreen.branch);
//            setTextInTextField("512664", accountManagementUI.transferMoneyScreen.account);
//            accountManagementUI.transferMoneyScreen.nextButton.click();
//            setTextInTextField("5000", accountManagementUI.transferMoneyScreen.amount);
//
//            //A click to close keyboard in case it is presented
//            accountManagementUI.transferMoneyScreen.dummyClick.click();
//        }
//
//        //continue btn
//        accountManagementUI.transferMoneyScreen.continueBtn.click();
//    }
//
//    //Step 2
//    @Test
//    public void D_transferMoneyTestStep3() {
//
//        waitForElement(accountManagementUI.transferMoneyScreen.btnBack, Constants.TIMEOUT);
//        accountManagementUI.transferMoneyScreen.btnBack.click();
//        waitForElement(accountManagementUI.transferMoneyScreen.continueBtn, Constants.TIMEOUT);
//        accountManagementUI.transferMoneyScreen.continueBtn.click();
//        waitForElement(accountManagementUI.transferMoneyScreen.btnBack, Constants.TIMEOUT);
//        accountManagementUI.transferMoneyScreen.btnConfirmTransfer.click();
//
//    }
//
//    //Step 3
//    @Test
//    public void E_transferMoneyTestStep4() {
//
//        waitForElement(accountManagementUI.transferMoneyScreen.btnClose, Constants.TIMEOUT);
//        accountManagementUI.transferMoneyScreen.btnClose.click();
//
//    }
//
//
//    public static void transferMoney(Method method,boolean ignoreFeild,String Id, String TransferTo, String Bank, String Branch, String Account, String Amount) {
//
//        List<WebElement> webElements = null;
//        //A list of all EditText fields on the screen
//        webElements = SharedUtilities.getWebElementsList(By.className(Id));
//
//        for (int i = 0; i < webElements.size(); i++) {
//            webElements.get(i).click();
//            switch (i) {
//                case 0:
//                    if (ignoreFeild){
//                        System.out.println("");
//                        return method;
//                        setTextInTextField (TransferTo, webElements.get (i));
//                    }else {
//                        System.out.println("");
//                    }
//                    break;
//                case 1:
//                    setTextInTextField(Bank, webElements.get(i));
//                    CloseKeyBoard();
//                    break;
//                case 2:
//                    setTextInTextField(Branch, webElements.get(i));
//                    break;
//                case 3:
//                    setTextInTextField(Account, webElements.get(i));
//                    CloseKeyBoard();
//                    break;
//                case 4:
//                    setTextInTextField(Amount, webElements.get(i));
//                    CloseKeyBoard();
//                    //stop the loop from running
//                    i = 222;
//                    break;
//            }
//        }
//    }
//}
