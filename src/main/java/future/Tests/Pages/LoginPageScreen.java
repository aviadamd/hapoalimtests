//package future.Tests.Pages;
//
//import org.openqa.selenium.WebElement;
//import future.BaseDK.BasicClass.BasicUiScreen;
//import io.appium.java_client.pagefactory.AndroidFindBy;
//import io.appium.java_client.pagefactory.iOSFindBy;
//import static General.Constants.ANDROID_ID;
//
//
//public class LoginPageScreen extends BasicUiScreen {
//
//
//    @iOSFindBy(id = "היכנס עכשיו")
//    @AndroidFindBy(id = ANDROID_ID + "welcome_start_btn")
//    public WebElement welcomeButton;
//
//    @iOSFindBy(id = "שכחתי סיסמא")
//    @AndroidFindBy(id = ANDROID_ID + "rl_business_main_activity")
//    public WebElement pageIdentifier;
//
//    @iOSFindBy(id = "FastBusinessLoginInputView_btnContinue")
//    @AndroidFindBy(id = ANDROID_ID + "sl_enterToAccountBtn")
//    public WebElement continueButton;
//
//    @iOSFindBy(id = "BL_PersonIcon")
//    @AndroidFindBy(id = ANDROID_ID +"user_name_input")
//    public WebElement userNameField;
//
//    @iOSFindBy(id = "BL_LockIcon")
//    @AndroidFindBy(id = ANDROID_ID + "password_input")
//    public WebElement passwordField;
//
//    @iOSFindBy(id = " ")
//    @AndroidFindBy(id = "android:id/button1")
//    public WebElement popupButton;
//
//    @iOSFindBy(id = "OK")
//    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
//    public WebElement permissionallowbutton;
//
//
//    @iOSFindBy(id = "אישור")
//    @AndroidFindBy(id = ANDROID_ID + "llFBAgree")
//    public WebElement confirmLegalButton;
//
//
//    @iOSFindBy(id = "תפריט מידע ופעולות")
//    @AndroidFindBy(className = "android.widget.ImageButton")
//    public WebElement menuButton;
//
//
//    /**
//     * taken from neol hechbon
//     */
//
//    //LOGIN SCREEN
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/enterAccountRL")
//    public WebElement userAccountR1;
//
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/sl_userNameInput")
//    public WebElement userNameInput;
//
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/sl_userPasswordInput")
//    public WebElement userNamePasswordInput;
//
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/sl_enterToAccountBtn")
//    public WebElement enterAccountButton;
//
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/smartLoginForgotPassword")
//    public WebElement forgotPassWordBtn;
//
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/smartLoginClose")
//    public WebElement goOnX_BackBtn;
//
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/sm_voiceRL")
//    public WebElement voiceRL;
//
//    @AndroidFindBy(id = "com.ideomobile.hapoalim:id/sm_onlyPassRL")
//    public WebElement onlyPassword;
//
//
//}
