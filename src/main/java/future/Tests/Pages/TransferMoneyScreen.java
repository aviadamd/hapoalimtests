//package future.Tests.Pages;
//
//import org.openqa.selenium.WebElement;
//
//import future.BaseDK.BasicClass.BasicUiScreen;
//import io.appium.java_client.pagefactory.AndroidFindBy;
//import io.appium.java_client.pagefactory.iOSFindBy;
//
//import static General.Constants.ANDROID_ID;
//
//public class TransferMoneyScreen extends BasicUiScreen {
//
//
//
//    @iOSFindBy(id = "כל הפעולות")
//    @AndroidFindBy(id = "btnMoreAction")
//    public WebElement allActions;
//
//    @iOSFindBy(id = "כל הפעולות והשירותים")
//    @AndroidFindBy(id = "actionMenuTitle")
//    public WebElement pageIdentifier;
//
//
//    @iOSFindBy(id = "העברת כספים")
//    @AndroidFindBy(className = "android.widget.Button")
//    public WebElement transferMoneyButton;
//
//
//    @iOSFindBy(id = "OK")
//    @AndroidFindBy(xpath = "//android.widget.Button[@index='1']")
//    public WebElement permissionallowbutton;
//
//
//    @iOSFindBy(id = "בחר מהעברות קודמות ומוטבים")
//    @AndroidFindBy(id = " ")
//    public WebElement beneficiariesButton;
//
//    @iOSFindBy(id = "ocavatar")
//    @AndroidFindBy(id = " ")
//    public WebElement selectCostumer;
//
//
//    @iOSFindBy(id = "כמה להעביר?")
//    @AndroidFindBy(xpath = " ")
//    public WebElement Quantity;
//
//    @iOSFindBy(tagName = "המשך")
//    @AndroidFindBy(xpath = " ")
//    public WebElement continueButton;
//
//    @iOSFindBy(id = "OCTransfer3rdPartyDetailsScreen_baneficiaryCell_txtField")
//    @AndroidFindBy(id = "one_click_beneficiary_text")
//    public WebElement transferToHom;
//
//    @AndroidFindBy(id = "wizard_edit_text_autocomplete")
//    public WebElement androidEditText;
//
//    @iOSFindBy(id = "OCTransfer3rdPartyDetailsScreen_bankCell_txtField")
//    @AndroidFindBy(id = ANDROID_ID + "one_click_bank_wizard")
//    public WebElement bank;
//
//    @iOSFindBy(id = "OCTransfer3rdPartyDetailsScreen_branchesCell_txtField")
//    @AndroidFindBy(id = ANDROID_ID + "one_click_branch_wizard")
//    public WebElement branch;
//
//    @iOSFindBy(id = "OCTransfer3rdPartyDetailsScreen_accountCell_txtField")
//    @AndroidFindBy(id = ANDROID_ID + "one_click_account_number")
//    public WebElement account;
//
//    @iOSFindBy(id = "OCTransfer3rdPartyDetailsScreen_amountCell_txtField")
//    @AndroidFindBy(id = ANDROID_ID + "one_click_transfer_amount")
//    public WebElement amount;
//
//    @iOSFindBy(id = "OCTransfer3rdPartyDetailsScreen_btnNext")
//    @AndroidFindBy(id = ANDROID_ID + "MWL_btnNext")
//    public WebElement continueBtn;
//
//    @iOSFindBy(id = "Next:")
//    @AndroidFindBy(id = ANDROID_ID + "MWL_btnNext")
//    public WebElement nextButton;
//
//    @iOSFindBy(id = "wiz_shadow_glila")
//    @AndroidFindBy(id = "wizard_navigation_steps_layout")
//    public WebElement dummyClick;
//
//    //step 2
//    @iOSFindBy(id = "OCTransfer3rdPartyManager_btnBack")
//    @AndroidFindBy(id = ANDROID_ID + "MWL_btnPrev")
//    public WebElement btnBack;
//
//    @iOSFindBy(id = "OCTransfer3rdPartyManager_btnConfirmTransfer")
//    @AndroidFindBy(id = ANDROID_ID + "MWL_btnNext")
//    public WebElement btnConfirmTransfer;
//
//    //step 3
//    @iOSFindBy(id = "OCTransfer3rdPartyManager_btnClose")
//    @AndroidFindBy(id = ANDROID_ID + "MWL_btnNext")
//    public WebElement btnClose;
//
//
//    @iOSFindBy(id = " ")
//    @AndroidFindBy(id = ANDROID_ID + "one_click_balance_text_to_withdrawl")
//    public WebElement balance;
//
//
//    @iOSFindBy(id = " ")
//    @AndroidFindBy(id = ANDROID_ID + "one_click_beneficiary_container")
//    public WebElement beneficiarycontainer;
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//}
